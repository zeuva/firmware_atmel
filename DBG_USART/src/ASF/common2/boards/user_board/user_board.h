/**
 * \file
 *
 * \brief User board definition template
 *
 */

 /* This file is intended to contain definitions and configuration details for
 * features and devices that are available on the board, e.g., frequency and
 * startup time for an external crystal, external memory devices, LED and USART
 * pins.
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef USER_BOARD_H
#define USER_BOARD_H

#include <conf_board.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \ingroup group_common_boards
 * \defgroup user_board_group User board
 *
 * @{
 */

void system_board_init(void);

/** Name string macro */
#define BOARD_NAME                "USER_BOARD"


#ifdef CONF_SPI
	#define SPI_MODULE SERCOM0
	#define SPI_MOSI PINMUX_PA04D_SERCOM0_PAD0
	#define SPI_SCK PINMUX_PA05D_SERCOM0_PAD1
	#define SPI_MISO PINMUX_PA07D_SERCOM0_PAD3
#endif

#ifdef CONF_FLASH
	#define DF_CS PIN_PA06
	#define DF_CS_ACTIVE               true
	#define DF_CS_INACTIVE             !DF_CS_ACTIVE
#endif

#ifdef CONF_I2C
	#define I2C_MODULE SERCOM2
	#define SDA PINMUX_PA08D_SERCOM2_PAD0
	#define SCL PINMUX_PA09D_SERCOM2_PAD1
#endif

#ifdef CONF_GSM
	#define GSM_MODULE SERCOM1
	#define GSM_RX PINMUX_PA16C_SERCOM1_PAD0
	#define GSM_TX PINMUX_PA17C_SERCOM1_PAD1
	#define GSM_PWRKEY				PIN_PA27
	#define GSM_PWRKEY_ACTIVE               true
	#define GSM_PWRKEY_INACTIVE             !GSM_PWRKEY_ACTIVE

#endif

#ifdef CONF_DEBUG
	#define DBG_MODULE SERCOM5
	#define DBG_TX PINMUX_PB02D_SERCOM5_PAD0
	#define DBG_RX PINMUX_PB03D_SERCOM5_PAD1
#endif

#ifdef CONF_SCI
	#define SCI_MODULE SERCOM4
	#define SCI_RX PINMUX_PB08D_SERCOM4_PAD0
	#define SCI_TX PINMUX_PB09D_SERCOM4_PAD1
	#define SCI_WAKE PIN_PA00
	#define CHARGING PIN_PB11
	#define DISCHARGING PIN_PB10
	
#endif

#ifdef CONF_EXTINT
	#define NFLTPIN PIN_PA01A_EIC_EXTINT1
	#define NFLTPINMUX MUX_PA01A_EIC_EXTINT1
#endif

/** @} */

#ifdef __cplusplus
}
#endif

#endif // USER_BOARD_H
