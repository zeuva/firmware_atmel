/*
 * emulated_eeprom.c
 *
 * Created: 06/01/2018 5:24:35 PM
 *  Author: Arjun
 */ 
#include "Internal_eeprom/emulated_eeprom.h"
#include <main.h>
void configure_eeprom(void)
{
    /* Setup EEPROM emulator service */
    enum status_code error_code = eeprom_emulator_init();
    if (error_code == STATUS_ERR_NO_MEMORY) {
        while (true) {
			user_debug("eeprom issue\r\n");
            /* No EEPROM section has been set in the device's fuses */
        }
    }
    else if (error_code != STATUS_OK) {
        /* Erase the emulated EEPROM memory (assume it is unformatted or
         * irrecoverably corrupt) */
        eeprom_emulator_erase_memory();
        eeprom_emulator_init();
    }
}


void backup_global_variable(uint8_t *glb_addr,uint16_t eeprom_addr,uint8_t var_sz,uint8_t flg)
{
	if(flg)
	{
		eeprom_emulator_write_buffer(eeprom_addr,glb_addr,var_sz);
		eeprom_emulator_commit_page_buffer();
	}
	else
	{
		eeprom_emulator_read_buffer(eeprom_addr,glb_addr,var_sz);
	}
}
