/*
 * emulated_eeprom.h
 *
 * Created: 06/01/2018 5:24:20 PM
 *  Author: Arjun
 */ 


#ifndef EMULATED_EEPROM_H_
#define EMULATED_EEPROM_H_
#include <asf.h>


#define EEPROM_WRITE	1
#define EEPROM_READ		0

void configure_eeprom(void);
void backup_global_variable(uint8_t *glb_addr,uint16_t eeprom_addr,uint8_t var_sz,uint8_t flg);






#endif /* EMULATED_EEPROM_H_ */