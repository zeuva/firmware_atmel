/*
 * interrupt.c
 *
 * Created: 1/11/2018 3:21:04 PM
 *  Author: Zeuva
 */ 
#include <interrupt.h>
#include <bms/pl455.h>
#include <guage/guage.h>
#include <timer/timeout.h>
struct tc_module dchgOT_instance;
struct tc_module chgOT_instance;
struct tc_module OC_instance;

uint16_t FLT_UV, maxCellDelta,maxCellNum;
uint16_t FLT_OV;
uint8_t soc;
int16_t ii;


void configure_extint_channel(void)
{
	struct extint_chan_conf config_extint_chan;
	extint_chan_get_config_defaults(&config_extint_chan);
	config_extint_chan.gpio_pin = NFLTPIN;								// #define NFLTPIN PIN_PA01
	config_extint_chan.gpio_pin_mux = NFLTPINMUX;						// #define NFLTPINMUX PIN_PA01A_EIC_EXTINT1
	config_extint_chan.gpio_pin_pull = EXTINT_PULL_UP;					// GPIO pulled up internally
	config_extint_chan.detection_criteria = EXTINT_DETECT_BOTH;			// A 'HIGH - LOW' level is detected
	extint_chan_set_config(1, &config_extint_chan);						// ???????
	//user_debug("Interrupt configured");
}

void configure_extint_callbacks(void)
{
	extint_register_callback(extint_detection_callback, 1 , EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(1,	EXTINT_CALLBACK_TYPE_DETECT);
	//user_debug("callback configured");
}void extint_detection_callback(void)			// nFLT Interrupt handler
{
	user_debug("Interrupt generated\r\n");
	uint8_t x =  port_pin_get_input_level(NFLTPIN);
	read_faults();
	if (x == 1)
	{
		user_debug("NFAULT pin 3V3\r\n");
		//port_pin_set_output_level(DISCHARGING, true);
		//port_pin_set_output_level(CHARGING, true);
		extint_chan_clear_detected(1);
	} 
	else
	{		
		user_debug("NFAULT pin 0V\r\n");
		//bool charge_state = port_pin_get_output_level(CHARGING);
		uint8_t discharge_state = port_pin_get_output_level(DISCHARGING);
		uint8_t charge_state = port_pin_get_output_level(CHARGING);
		user_debug("Charge status =%d  Discharge status =%d \r\n ",charge_state,discharge_state);
		//if (ii > 0)
		//{
			//port_pin_set_output_level(DISCHARGING, false);
			if (FLT_OV > 0)
			{
				user_debug("Over voltage fault\r\n");
				Balance_ON(FLT_OV);				
			}
			else if (FLT_AUX > 0)
			{
				user_debug("Over Temperature fault in charging condition\r\n");
				tc_enable(&chgOT_instance);		// Start Timer for 10 sec to check if Over Temp Fault still persists
								
			}
			else if ( FLT_OV == 0 && FLT_AUX == 0)
			{
				user_debug("Fault due to something else\r\n");
				//Charging_OFF();
				read_faults();
			}
		
			else if (FLT_UV > 0)
			{
				user_debug("Under voltage fault\r\n");
				Discharge_OFF();
				Balance_OFF();
			}
			else if (ii < -1000)
			{
				user_debug("Over current fault\r\n");
				Discharge_OFF();
			}
			else if (FLT_AUX > 0)
			{
				user_debug("Over Temperature fault in Discharge condition\r\n");
				tc_enable(&dchgOT_instance);		// Start Timer for 10 sec to check if Over Temp Fault still persists
				//Discharge_OFF();
			}
			else if ( ii < 24000)
			{
				tc_enable(&OC_instance);		// Start 10 seconds timer and check if over current still persists
				
			}
			else
			{
				user_debug("Fault due to something else\r\n");
				Discharge_OFF();
				//Charging_OFF();
				read_faults();
			}
		}
						
		extint_chan_clear_detected(1);
	}
	
	/*
	bool fault_state = port_pin_get_input_level(NFLTPIN);
	bool charge_state = port_pin_get_output_level(CHARGING);
	bool discharge_state = port_pin_get_output_level(DISCHARGING);
	delay_ms(10);
	user_debug("Fault status =%d  Charge status =%d  Discharge status =%d \r\n ", fault_state,charge_state,discharge_state);
	if (fault_state == false && charge_state == true)
	{
		if (FLT_OV > 0)
		{
			user_debug("Over voltage fault\r\n");
			Balance_ON(FLT_OV);
		} 
		else if (FLT_AUX > 0)
		{
			user_debug("Over Temperature fault in charging condition\r\n");
			Charging_OFF();
		}
		else
		{
			user_debug("Fault due to something else\r\n");
			Charging_OFF();			
		}
	} 
	else if (fault_state == false && discharge_state == true)
	{
		if (FLT_UV > 0)
		{
			user_debug("Under voltage fault\r\n");
			Discharge_OFF();
		} 
		else if (ii < -1000)
		{
			user_debug("Over current fault\r\n");
			Discharge_OFF();
		}
		else if (FLT_AUX > 0)
		{
			user_debug("Over Temperature fault in Discharge condition\r\n");
			Discharge_OFF();
		}
		else
		{
			user_debug("Fault due to something else\r\n");
			Discharge_OFF();
		}
	}
	else if (fault_state == false)
	{
		user_debug("Fault has occured due to some other reason\r\n");
	}
	else
	{
		Charging_ON();
		Discharge_ON();
	}
	*/
//}void identify_scenario(){
	
	if (FLT_OV > 0 && soc > 80 && ii < 100)
	{
		user_debug("FLT_OV > 0 && soc > 80 && ii < 100 \r\n");
		/*while(pin_state == 0)
		{
			Balance_ON(FLT_OV);
			Charging_ON();
		}*/
	}
	else if (FLT_OV > 0)
	{
		user_debug("FLT_OV > 0\r\n");
		/*while (pin_state == 0)
		{
			Balance_ON(FLT_OV);
		}*/
		
	}	else if (FLT_OV == 0 && soc < 90)	{		Charging_ON();	}	else if (FLT_UV > 0 && soc < 20)
	{
		user_debug("FLT_UV > 0 && soc < 20\r\n");
		OverDischarge();
	}	else if (FLT_UV > 0)
	{
		user_debug("FLT_UV > 0\r\n");
		Balance_OFF();
		Discharge_OFF();
		Charging_ON();
		
	}	else if (soc < 20)	{		user_debug("NO charge inside battery \r\n");	}	else if (maxCellDelta > 200)
	{
		user_debug("maxCellDelta > 200\r\n");
		cellVoltmeasurement();
		Balance_ON(maxCellNum);
	}	else	{		user_debug("Normal condition\r\n");		Balance_OFF();						// Cell Balancing OFF		Discharge_ON();						// Discharge MOSFET ON		mask_checksum_fault();				//	clear faults	}}