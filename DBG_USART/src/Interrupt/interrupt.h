/*
 * interrupt.h
 *
 * Created: 1/11/2018 3:21:23 PM
 *  Author: Zeuva
 */ 


#ifndef INTERRUPT_H_
#define INTERRUPT_H_

void extint_detection_callback(void);
void configure_extint_callbacks(void);
void configure_extint_channel(void);
void identify_scenario();



#endif /* INTERRUPT_H_ */