#include <math.h>
#include "pl455.h"
#include <main.h>
CELLVOLT cv_obj;

uint8_t rec_data;
int nSent;
uint8_t nRead, nTopFound, nDev_ID = 0;
uint8_t UART_RX_RDY, RTI_TIMEOUT = 0;
uint16_t FLT_summ, FLT_UV, FLT_OV, FLT_COMUV, FLT_COMOV, FLT_OPCTR, Sr_num, FLT_SYS, FLT_OT,FLT_AUX = 0;
uint16_t FLT_COMM, FLT_CHIP, channel_slct, devconfig, reg_val, num_FLT_OV = 0, num_FLT_UV = 0 , num_FLT_AUX = 0;
// internal function prototype
uint16_t CRC16(char *pBuf, int nLen);

void pl455_port_init()
{
	#ifdef CONF_SCI
	struct port_config pin_conf;
	port_get_config_defaults(&pin_conf);
	
	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(SCI_WAKE, &pin_conf);
	port_pin_set_output_level(SCI_WAKE, false);

	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(CHARGING, &pin_conf);
	port_pin_set_output_level(CHARGING, false);

	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(DISCHARGING, &pin_conf);
	port_pin_set_output_level(DISCHARGING, false);
	#endif
}

void configure_SCI_uart()
{
	#ifdef CONF_SCI
	uint8_t sci_instance_index;
	struct usart_config usart_conf;
	
	pl455_port_init();
	
	usart_get_config_defaults(&usart_conf);
	usart_conf.mux_setting = USART_RX_1_TX_0_XCK_1;
	usart_conf.pinmux_pad0 = SCI_RX;
	usart_conf.pinmux_pad1 = SCI_TX;
	usart_conf.pinmux_pad2 = PINMUX_UNUSED;
	usart_conf.pinmux_pad3 = PINMUX_UNUSED;
	usart_conf.baudrate    = SCI_BAUDRATE;
	while (usart_init(&sci_uart_module, SCI_MODULE, &usart_conf) != STATUS_OK) {
	}
	
	// Enable the UART transceiver
	usart_enable(&sci_uart_module);
	usart_enable_transceiver(&sci_uart_module, USART_TRANSCEIVER_TX);
	usart_enable_transceiver(&sci_uart_module, USART_TRANSCEIVER_RX);
	#endif
}

void WakePL455(void)
{
	#ifdef CONF_SCI
	// toggle wake signal
	port_pin_set_output_level(SCI_WAKE, false); // assert wake (active low)
	delay_us(200);
	port_pin_set_output_level(SCI_WAKE, true);
	delay_ms(1000);
	#endif
}

void Charging_ON()
{
	#ifdef CONF_SCI
	//port_pin_set_output_level(CHARGING, true);
	port_pin_set_output_level(CHARGING, false);	//for sawant sir new board logic is opposite
	#endif
}

void Charging_OFF()
{
	#ifdef CONF_SCI
	//port_pin_set_output_level(CHARGING, false);
	port_pin_set_output_level(CHARGING, true);	//for sawant sir new board logic is opposite
	#endif
}

void Discharge_ON()
{
	#ifdef CONF_SCI
	port_pin_set_output_level(DISCHARGING, true);
	#endif
}

void Discharge_OFF()
{
	#ifdef CONF_SCI
	port_pin_set_output_level(DISCHARGING, false);
	#endif
}

void Balance_OFF()
{
	nSent = WriteReg(nDev_ID, CBENBL, 0x0000, 2, FRMWRT_SGL_NR);        // Balancing Disabled FETs for 6 channels
}

void Balance_ON(uint16_t ovf)
{
	nSent = WriteReg(nDev_ID, CBCONFIG, 0x28, 1, FRMWRT_SGL_NR);          // Balancing enabled for 2 minutes
	// Balancing does not stop if any fault bit is set
	nSent = WriteReg(nDev_ID, CBENBL, ovf, 2, FRMWRT_SGL_NR);		  // Balancing Enabled FETs for 13 channels
}

void Read_Bal_Reg()
{
	uint8_t Balconfig;
	uint16_t Balenbl;
	nRead = ReadReg(nDev_ID, CBCONFIG, &Balconfig, 1, 0);
	nRead = ReadReg(nDev_ID, CBENBL, &Balenbl, 2, 0);
	user_debug("Balance config: %d",Balconfig);
	user_debug("Balance Enable: %d",Balenbl);
}

void Overcharge()
{
	Charging_OFF();                                                 // Disconnect Charging Mosfet
	Balance_ON(FLT_OV);                                             // Start passive balancing
	Discharge_ON();													// Motor side Mosfet should be connected again for running vehicle
}

void OverDischarge()
{
	Discharge_OFF();                                                // Disconnect Motor side Mosfet
	Balance_OFF();                                                  // Stop passive balancing
	Charging_ON();													// Charger side Mosfet is Turned ON for charging to occur
}

void Overcurrent()
{
	Discharge_OFF();                                                // Disconnect Discharging Mosfet
	Balance_OFF();                                                  // Stop passive balancin
}

void bq76pl455_init()
{
	mask_checksum_fault();											// Clear all the faults there in the BMS bq76pl455 IC
	uart_comm();													// Baud rate selection for UART communication between bq76pl455 and SAMD20G18
	power_config();													// Power configuration as per data sheet
	device_config();
	device_control();
	device_add();
	configure_AFE();
	Balance_OFF();
}

void mask_checksum_fault()
{	
	nSent = WriteReg(0, MASK_DEV, 0x8000, 2, FRMWRT_SGL_NR);               // User checksum error mask
	delay_ms(5);														   // This delay is important for Clearing Sys_Reset fault
	nSent = WriteReg(0, FAULT_SYS, 0x80, 1, FRMWRT_SGL_NR);				   // System_Reset fault clear Write
	delay_us(500);
	nSent = WriteReg(0, DEV_STATUS, 0x18, 1, FRMWRT_SGL_NR);               // clear fault flags in the system status register
	//nSent = WriteReg(0, 82, 0xFFC0, 2, FRMWRT_SGL_NR);				   // clear all fault summary flags
	delay_us(100);	
}

void Read_debug(uint16_t register_num)
{
	nRead = ReadReg(nDev_ID, register_num, &channel_slct, 1, 0);
	user_debug("Selected register %d Value is: %d\r\n",register_num, channel_slct);
}
void checksum_write()
{
	uint32_t chksum, chksumstr = 0;
	nRead = ReadReg(nDev_ID, CSUM_RSLT, &chksum, 4, 0);    				    // This register contains the current internally calculated checksum for the registers.
	nSent = WriteReg(nDev_ID, CSUM, chksum, 4, FRMWRT_SGL_NR);				// This register contains the programmed checksum for the registers.
	nRead = ReadReg(nDev_ID, CSUM, &chksumstr, 4, 0);
	nSent = WriteReg(0, MASK_DEV, 0x8000, 2, FRMWRT_SGL_NR);                // User checksum error mask
	//delay_ms(5);															// This delay is important for Clearing Sys_Reset fault
	//nSent = WriteReg(0, 96, 0x80, 1, FRMWRT_SGL_NR);						// System_Reset fault clear Write
	delay_us(500);
	nSent = WriteReg(0, DEV_STATUS, 0x18, 1, FRMWRT_SGL_NR);                // clear fault flags in the system status register
	delay_us(100);
	nSent = WriteReg(0, FAULT_SUM, 0xFFC0, 2, FRMWRT_SGL_NR);               // clear all fault summary flags
	delay_us(100);
}
void uart_comm()
{
	nSent = WriteReg(nDev_ID, COMCONFIG, 0x1080, 2, FRMWRT_SGL_NR);         // set communications baud rate as 250KBaud
}

void read_tx_holdoff()
{
	uint8_t data;
	nRead = ReadReg(nDev_ID, TXHOLDOFF, &data, 1, 3);						// set communications baud rate as 250KBaud
}

void write_tx_holdoff()
{
	nSent = WriteReg(nDev_ID, TXHOLDOFF, 0xff, 1, FRMWRT_SGL_NR);
}

void serial_num()
{
	nRead = ReadReg(nDev_ID, SER_NUM, &Sr_num, 2, 0);						// Read serial number of BQ76 IC
}

void channel_sel()
{
	nSent = WriteReg(nDev_ID, NCHAN, 0x0D, 1, FRMWRT_SGL_NR);				// NCHAN : set number of cells to 13
	nSent = WriteReg(nDev_ID, CHANNELS, 0x1FFFFF02, 4, FRMWRT_SGL_NR);		// select all 13 cell, AUX channels 6 AND 8, and internal digital die and internal analog die temperatures
	nSent = WriteReg(nDev_ID, AM_PER, 0x04, 1, FRMWRT_SGL_NR);				// Auto monitor period 10ms
	nSent = WriteReg(nDev_ID, AM_CHAN, 0x1FFFFF00, 4, FRMWRT_SGL_NR);		// Auto monitor channels
}

void power_config()
{
	nSent = WriteReg(nDev_ID, PWRCONFIG, 0x80, 1, FRMWRT_SGL_NR);			// standard configuration as per data sheet
}

void device_config()
{
	nSent = WriteReg(nDev_ID, DEVCONFIG, 0x11, 1, FRMWRT_SGL_NR);           // Faults are unlatched and clear automatically,
	// Comparator hysteresis is disabled.
	// Over voltage (OV) and under voltage (UV) comparators are enabled.
	// Address will be set using Auto Addressing.
	// Internal regulator (NPN drive for VP/VDIG) is enabled. This is the normal operating mode.
}
void device_control()
{
	nSent = WriteReg(nDev_ID, DEV_CTRL, 0x08, 1, FRMWRT_SGL_NR);			// Auto Address enable
}
void device_add()
{
	uint32_t chksum = 0;
	nSent = WriteReg(nDev_ID, ADDR, 0x00, 1, FRMWRT_SGL_NR);				// Device address = 00
	nRead = ReadReg(nDev_ID, CSUM_RSLT, &chksum, 4, 0);    					// This register contains the current internally calculated checksum for the registers.
	nSent = WriteReg(nDev_ID, CSUM, chksum, 4, FRMWRT_SGL_NR);				// This register contains the programmed checksum for the registers.
	nSent = WriteReg(nDev_ID, MASK_DEV, 0x8000, 2, FRMWRT_SGL_NR);          // User checksum error mask
	delay_us(500);
	nSent = WriteReg(nDev_ID, DEV_STATUS, 0x18, 1, FRMWRT_SGL_NR);          // clear fault flags in the system status register
	delay_us(100);
	nSent = WriteReg(nDev_ID, FAULT_SUM, 0xFFC0, 2, FRMWRT_SGL_NR);         // clear all fault summary flags
	nSent = WriteReg(nDev_ID, FAULT_UV, 0xFFFF, 2, FRMWRT_SGL_NR);          // UV clear
	nSent = WriteReg(nDev_ID, FAULT_OV, 0xFFFF, 2, FRMWRT_SGL_NR);          // OV clear
	nSent = WriteReg(nDev_ID, FAULT_AUX, 0xFFFF, 2, FRMWRT_SGL_NR);         // AUX clear
	nSent = WriteReg(nDev_ID, FAULT_2UV, 0xFFFF, 2, FRMWRT_SGL_NR);         // COMP UV  clear
	nSent = WriteReg(nDev_ID, FAULT_2OV, 0xFFFF, 2, FRMWRT_SGL_NR);         // COMP OV  clear
	nSent = WriteReg(nDev_ID, FAULT_COM, 0xFFE9, 2, FRMWRT_SGL_NR);         // COMM FLT clear
	nSent = WriteReg(nDev_ID, FAULT_SYS, 0xFF, 1, FRMWRT_SGL_NR);           // SYSFLT clear
	nSent = WriteReg(nDev_ID, FAULT_DEV, 0xFFFF, 2, FRMWRT_SGL_NR);         // Chip clear
	nSent = WriteReg(nDev_ID, FAULT_GPI, 0x3F, 1, FRMWRT_SGL_NR);           // GPIO FAULT CLEAR
	nSent = WriteReg(nDev_ID, MASK_COMM, 0xFFE9, 2, FRMWRT_SGL_NR);         // mask comm flt
	nSent = WriteReg(nDev_ID, MASK_SYS, 0xDF, 1, FRMWRT_SGL_NR);            // mask sys flt
	nSent = WriteReg(nDev_ID, MASK_DEV, 0xE000, 2, FRMWRT_SGL_NR);          // Mask chip fault
}
void fault_output()
{   user_debug("In fault_output \n ");    // edited 31-12-2018
	nSent = WriteReg(nDev_ID, FO_CTRL, 0xFF00, 2, FRMWRT_SGL_NR);			// Fault_Output control
}
void configure_AFE()
{
	nSent = WriteReg(nDev_ID, SMPL_DLY1, 0x00, 1, FRMWRT_SGL_NR);			// initial sampling delay
	nSent = WriteReg(nDev_ID, CELL_SPER, 0x44, 1, FRMWRT_SGL_NR);			// voltage and Temp sampling interval 12.6us
	nSent = WriteReg(nDev_ID, AUX_SPER, 0x44444444, 4, FRMWRT_SGL_NR);		// initial sampling delay
	nSent = WriteReg(nDev_ID, OVERSMPL, 0x00, 1, FRMWRT_SGL_NR);			// Oversampling rate and command
	nSent = WriteReg(nDev_ID, DEV_STATUS, 0x38, 1, FRMWRT_SGL_NR);          // clear fault flags in the system status register
	nSent = WriteReg(nDev_ID, FAULT_SUM, 0xFFC0, 2, FRMWRT_SGL_NR);         // clear all fault summary flags
	channel_sel();
	Voltage_threshold();													// Cell OV,UV,Comp_OV, Comp_UV voltage threshold setting
	Thermistor_threshold();													// Auxiliary thermistors Upper and Lower limit threshold setting
}

/* Cell Upper and Lower threshold values */
/*
***	Over voltage threshold:: Let Over Voltage threshold = 4.2 Volts
Threshold value is set like							          5V      :   65535
*                                  4.20    :   ?(X)
*                                  X = (4.20 x 65535)/5
*                                  X = 55049 DECIMAL
*                                  Convert X to HEXADECIMAL
*                                  X = D709								"X=b333 for 3.5 V"

*** Under Voltage threshold:: Let Under Voltage threshold = 3.00 Volts
Threshold value is set like									  5V      :   65535
*                                  3.00    :   ?(X)
*                                  X = (3.00 x 65535)/5
*                                  X = 39321 DECIMAL
*                                  Convert X to HEXADECIMAL
*                                  X = 9999

***	Comparator Under Voltage threshold:: Let Comp_UV threshold = 3.00 Volts
*  CMP_UV_THRES  :  These bits set the comparator undervoltage-threshold value in 25-mV steps. The range is determined
by the CMP_TST_SHF_UV bit.
*  CMP_TST_SHF_UV : This bit sets the operating range for the undervoltage comparators.
0 = Normal range of 0.7 V to 3.875 V
1= Shifted range of 2.0 V to 5.175 V (used for self-test purposes)
Here we will go for normal range of 0.7 V to 3.875 V. So, the actual range is 3.875 - 0.7 = 3.175
Voltage value moves in the steps of 25mV. Therefore total steps  = 3.175/0.025 = 127 steps
Let my COMP_UV threshold = 3.000V therefore steps = (3.00 - 0.7) / 0.025 =  92 steps
Convert 92 into Hexadecimal : 0x5C
Left shift by 1 = = 0xB8

*** Comparator Over Voltage threshold:: Comp_OV threshold = 4.25 Volts
*  CMP_OV_THRES  :  These bits set the comparator Overvoltage-threshold value in 25-mV steps. The range is determined
by the CMP_TST_SHF_UV bit.
*  CMP_TST_SHF_OV : This bit sets the operating range for the undervoltage comparators.
0 = Normal range of 2.0 V to 5.175 V
1= Shifted range of 0.7 V to 3.875 V (used for self-test purposes).
Here we will go for normal range of 2.0 V to 5.175 V. So, the actual range is 5.175 - 2.0 = 3.175
Voltage value moves in the steps of 25mV. Therefore total steps  = 3.175/0.025 = 127 steps
Let my COMP_UV threshold = 3.000V therefore steps = (4.25 - 2.00) / 0.025 =  90 steps
Convert 80 into Hexadecimal : 0x5A
Left shift by 1. = = 0xB4

*/
void Voltage_threshold()
{
	//NOTE:scale volt*16383/5 to get 14 bit number and then left shit by 2 bits to get 16 bit number with last 2 bits as 00
	nSent = WriteReg(nDev_ID, CELL_OV, 0xCCCC, 2, FRMWRT_SGL_NR);         // Cell overvoltage threshold        "value changed from D709 to b333; c28e for 3.8; c7ad for 3.9; CCCC ~ 4.0V
	nSent = WriteReg(nDev_ID, CELL_UV, 0xA8F4, 2, FRMWRT_SGL_NR);         // Cell undervoltage threshold (3.2V)  value for 3 is 9999; b333 ~ 3.5V; a3d6 ~ 3.2V; a665 ~ 3.25V; A8F5 ~ 3.3V but use A8F4 as last 2 bits has to be 00;
	nSent = WriteReg(nDev_ID, COMP_OV, 0xB4, 1, FRMWRT_SGL_NR);           // Cell comparator overvoltage threshold
	nSent = WriteReg(nDev_ID, COMP_UV, 0xB8, 1, FRMWRT_SGL_NR);           // Cell comparator undervoltage threshold
}


/* Thermistors Upper and Lower threshold values */
/*
Thermistor Overvoltage threshold:: Let thermistor over voltage threshold :: 4.00 Volts
This register contains the Over voltage threshold that will be used for the AUX samples. This is a
scaled offset-binary value from 0 V to 5 V.
5V	:	16384
4.0	:	? (X)
X = (4.0 * 16384)/5
X = 13102 decimal
Convert X into Hexadecimal and left shift by 2
X_hex = CCB8

Thermistor Undervoltage threshold:: Let thermistor Under voltage threshold :: 2.00 Volts
This register contains the Over voltage threshold that will be used for the AUX samples. This is a
scaled offset-binary value from 0 V to 5 V.
5V	:	16384
2.0	:	? (X)
X = (2.0 * 16384)/5
X = 6553 decimal
Convert X into Hexadecimal and left shift by 2
X_hex = 6664
*/

void Thermistor_threshold()
{
	//NOTE:scale volt*16383/5 to get 14 bit number and then left shit by 2 bits to get 16 bit number with last 2 bits as 00
	uint64_t val_ov=0x7ffc;//temp below threshold ==> high R ==> over voltage
	uint64_t val_uv=0x6f74;//temp above threshold ==> low R ==> under voltage
	//7ffc ~ 2.5V for 27C RTH 10K  
	//6f74 ~ 2.177V for 34C RTH 5.827K
	nSent = WriteReg(nDev_ID, AUX0_UV, val_uv, 2, FRMWRT_SGL_NR);           // Auxillary 0 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX0_OV, val_ov, 2, FRMWRT_SGL_NR);           // Auxillary 0 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX1_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 1 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX1_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 1 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX2_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 2 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX2_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 2 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX3_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 3 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX3_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 3 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX4_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 4 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX4_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 4 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX5_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 5 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX5_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 5 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX6_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 6 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX6_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 6 overvoltage threshold
	nSent = WriteReg(nDev_ID, AUX7_UV, 0x0000, 2, FRMWRT_SGL_NR);           // Auxillary 7 undervoltage threshold
	nSent = WriteReg(nDev_ID, AUX7_OV, 0xffff, 2, FRMWRT_SGL_NR);           // Auxillary 7 overvoltage threshold
	
}

uint32_t module_voltage;

void readvoltage()
{
	char bFrame[132];
	uint16_t volt,a,b,c,R,TK,Tc;
	uint8_t i, i2, cnt_ov_release = 0, cnt_uv_release = 0, cnt_uv_th = 0,cnt_ut_release=0, cnt_ot_release=0;
	uint32_t calc_val;
	bool charging_on_ok_by_V = false;
	bool discharging_on_ok_by_V = false;
	bool on_ok_by_T = false;
	bool any_cell_fault = false;
	module_voltage = 0;											//commented on 2/1/2019
	double x,y;
	uint16_t *ptr_cv = &m_obj.bd.bms_cv;
	uint16_t *ptr_ct = &m_obj.bd.bms_ct;
	channel_sel();
	
	// Send request to board 1 to sample and store results
	nSent = WriteReg(nDev_ID, CMD, 0x00, 1, FRMWRT_SGL_NR);					// send sync sample and store command

	delay_us(2800);					//changed from 2800 to 3500										// still need to wait for sampling to complete

	// Read stored sample data from boards
	nSent = WriteReg(nDev_ID, CMD, 0x20, 1, FRMWRT_SGL_R);					// send read stored values command
	nSent = WaitRespFrame(bFrame, 50, 4);									// 32 bytes data + packet header + CRC, 0ms timeout
	
	for (i=1;i<22;i++)
	{
		if (i==1)
		{
			for (i2=1;i2<=13;i2++)
			{
				user_debug("%d\t",i2);       //change made in volt 20/12/2018 added
			}
			user_debug("\r\n");       //change made in volt 20/12/2018 added
		}
		
		if (i==14)
		{
			user_debug("\r\n");
			for (i2=1;i2<=8;i2++)
			{
				user_debug("%dV\t%dT\t",i2);       //change made in volt 20/12/2018 added
			}
			user_debug("\r\n");       //change made in volt 20/12/2018 added
		}
		
		//volt = (bFrame[(i*2-1)]<<8) | bFrame[i*2] ;  // bFrame[1] << 8 | bFrame[2] //modifed added 26 & 28 - 20/12/18
		//user_debug("Val1: %02x, Val2: %02x\r\n", (int)bFrame[i*2-1], (int)bFrame[i*2]);
		//calc_val = (volt * 4994)/65535;							//used to convet into voltage from HEX value.  20/12/2018
		//		user_debug("voltage %d: %d\t",i, calc_val);       //change made in volt 20/12/2018 added
		//		user_debug("%d\t", calc_val);       //change made in volt 20/12/2018 added ; commented on 16/1/2019
		//calc_val = (volt * 4994)/65535;					//commented on 20/12/2018
		//		user_debug("address: %d\r\n", (uint16_t)ptr);
		if (i<14)
		{
			volt = (bFrame[26-(i*2-1)]<<8) | bFrame[28-i*2];				  //edited on 17/1/2019
			calc_val = (volt * 4994)/65535;									//edited on 17/1/2019
			user_debug("%d\t", calc_val);
			*ptr_cv++ = (uint16_t)calc_val;
			
			//if(calc_val < 2600)											//written value 3400 instead of 2600;
			//{
				//any_cell_fault = true;
			//}
			if(calc_val < 3750)											//added on 2/1/2019;// upper release
			{
					cnt_ov_release++;
			}
			if(calc_val > 3400)											// lower release
			{
					cnt_uv_release++;	
			}															//added upto this on 2/1/2019			
			module_voltage += calc_val;
			//user_debug("Voltage:%d %d\r\n",i, calc_val);
		}
		else
		{
			/*	From BMS1 schematics for auxilliary thermistors, there is a voltage divider
			Vp = 5.3 V ;
			Vaux = (Rth+10) / (Rth + 10 + 10k) x Vp ;
			Rth = [{(calc_val*10010)/1000 - 53} x 1000] / (5300 - calc_val);
			B = 3435 K ( from 103AT-2 datasheet)
			R~ = R25 x e^(-B/T25)
			T = B / ln (Rth/R~)
			*/
			volt = (bFrame[68-(i*2-1)]<<8) | bFrame[70-i*2];               //edited on 17/1/2019
			calc_val = (volt * 4994)/65535;									//edited on 17/1/2019
			a = (calc_val*10010)/1000;
			b = a - 53;
			c = (5300 - calc_val);
			R = b*1000/c;
			x = 102*R/10;
			y = log(x);
			TK = 3435/y;
			Tc = TK - 273;
			user_debug("%d\t%d\t", calc_val, Tc);
			//user_debug("Temperature :%d = %d\r\n",i,Tc);			 //used to display temperature; uncommented on 16/1/2019
			*ptr_ct++ = (uint16_t)Tc;
			if(Tc < 34)											//added on 2/1/2019;// upper release
			{
				cnt_ot_release++;
			}
			if(Tc > 27)											// lower release
			{
				cnt_ut_release++;
			}															//added upto this on 2/1/2019
		}
		volt = 0;
		
	}
	
	user_debug("\r\n\nModule Voltage: %d\r\n\n",  module_voltage);
	
	if (cnt_ot_release==1 && cnt_ut_release==8)
	{
		on_ok_by_T=true;
		user_debug("All cells reached T release\r\n");
	}
	if(cnt_ov_release==13)
	{
		charging_on_ok_by_V=true;
		user_debug("All cells reached over V release\r\n");
	}
	if(cnt_uv_release==13)
	{
		discharging_on_ok_by_V=true;
		user_debug("All cells reached under V release\r\n");
	}
	
	if(on_ok_by_T && charging_on_ok_by_V)
	{
		Charging_ON();
		user_debug("charging can be turned ON\r\n");
	}
	if(on_ok_by_T && discharging_on_ok_by_V)
	{
		Discharge_ON();
		user_debug("discharging can be turned ON\r\n");
	}
	
	user_debug("\r\n");
	
	/*if (module_voltage >= 53300 && (!any_cell_fault))	    // Module voltage should be greater than 54.6V to switch off charging pin
	{																			//commented on 2/1/2019
	user_debug("Turn off Charging, Turn on Discharging\r\n");
	Charging_OFF();
	Discharge_ON();
	}
	else if ((module_voltage > 41000) && (module_voltage < 51000) && (!any_cell_fault))
	{
	user_debug("Turn on Charging, Turn on Discharging\r\n");
	Charging_ON();
	Discharge_ON();
	}
	else if (module_voltage < 39500 && any_cell_fault)
	{
	user_debug("Turn on Charging, Turn off Discharging\r\n");
	Charging_ON();
	Discharge_OFF();
	}*/

}

void cellVoltmeasurement()
{
	uint16_t V_temp;
	uint32_t cellSample, cellAverage, minCellDelta, maxCellDelta, minCell, maxCell;
	uint8_t cellToBalance, maxCellNum, minCellNum, incCount, decCount;
	char bFrame[132];														//20/12/2018 "132 bytes allocated for bFRAME"
	
	channel_sel();
	
	//  * Find the max and min cell channels and their voltages. It's pretty self-explanatory ;-)

	// Initialize variables
	maxCell = 0;
	minCell = 3;
	maxCellNum = 0;
	minCellNum = 0;
	maxCellDelta = 0;
	minCellDelta = 0;
	cellAverage = 0;
	cellToBalance = 0;
	cellSample = 0;
	
	// Send request to board 1 to sample and store results
	nSent = WriteReg(nDev_ID, CMD, 0x00, 1, FRMWRT_SGL_NR);						// send sync sample and store command

	delay_us(2800);																// still need to wait for sampling to complete

	// Read stored sample data from boards
	nSent = WriteReg(nDev_ID, CMD, 0x20, 1, FRMWRT_SGL_R);						// send read stored values command
	nSent = WaitRespFrame(bFrame, 35, 4);										// 32 bytes data + packet header + CRC, 0ms timeout

	for(decCount = 13, incCount = 4; incCount < 17; incCount ++, decCount--)
	{
		
		V_temp = (bFrame[incCount*2-1]<<8|bFrame[incCount*2]) ;
		cellSample = (V_temp * 5000)/65535;
		//user_debug("voltage[%d] : %d\r\n", decCount, cellSample);
		
		if(incCount == 4)
		{
			maxCell = cellSample;
			minCell = cellSample;
			maxCellNum = 13;
			minCellNum = 13;
			cellAverage = cellSample;
		}
		else
		{
			cellAverage += cellSample;
			if(cellSample > maxCell)
			{
				maxCell = cellSample;
				maxCellNum = decCount;
			}
			else
			{
				if(cellSample < minCell)
				{
					minCell = cellSample;
					minCellNum = decCount;
				}
			}
		}
	}
	cellAverage /= 13;
	maxCellDelta = maxCell - cellAverage;
	minCellDelta = cellAverage - minCell;
	//user_debug("Max cell value : %d\r\n", maxCell);
	//user_debug("Min cell value : %d\r\n", minCell);
	//user_debug("Max Volt cell number : %d\r\n", maxCellNum);
	//user_debug("Min Volt cell number : %d\r\n", minCellNum);
	//user_debug("Cell Volt Average %d\r\n", cellAverage);
	//user_debug("Maximum cell voltage delta %d\r\n", maxCellDelta );
	//user_debug("Minimum cell voltage delta %d\r\n",minCellDelta);
	delay_ms(1);
}
/* Read Faults on the BMS bq76pl455 IC  */
/*
Fault summary bits significance:: Address 82,83
[15] UV_FAULT_SUM
[14] OV_FAULT_SUM
[13] AUXUV_FAULT_SUM
[12] AUXOV_FAULT_SUM
[11] CMPUV_FAULT_SUM
[10] CMPOV_FAULT_SUM
[9] COMM_FAULT_SUM
[8] SYS_FAULT_SUM
[7] CHIP_FAULT_SUM
[6] GPI_FAULT_SUM
[5:0] RSVD
For each of these bits:
Write '1': Reset all fault conditions of this type.
Write '0': No effect
Read '1': One or more of the individual fault bits of this type are currently set.
These bits always reflect the state of the underlying bits in the other fault registers, which may be
latched or not, depending on the setting of the DEVCONFIG[UNLATCHED_FAULT] bit.

Under voltage fault:: Address 84,85
[15:0] UV_FAULT
Write '1': Reset the fault condition.
Write '0': No effect
Read '1': The stored result for the corresponding battery channel is less than UV_THRES_CELL.
UV_FAULT[0] corresponds to cell 1.
If UNLATCHED_FAULT is set, this register is self-clearing.

Over voltage fault:: Address 86,87
[15:0] OV_FAULT
Write '1': Reset the fault condition.
Write '0': No effect
Read '1': The stored result for the corresponding battery channel is greater than
OV_THRES_CELL. OV_FAULT[0] corresponds to cell 1.
If UNLATCHED_FAULT is set, this register is self-clearing.

Auxiliary thermistor faults:: Address 88,89
[15:8] AUX_UV_FAULT
[7:0] AUX_OV_FAULT
For each bit in this bitmask:
Write '1': Reset the fault condition.
Write '0': No effect
Read '1': The stored result for the corresponding auxiliary channel is greater than
auxiliary threshold
If UNLATCHED_FAULT is set, this register is self-clearing.

*/

void read_all_faults()
{
	uint8_t bFrame[30];
	uint16_t *ptr_bf = &m_obj.bd.bms_bf;
	nRead = WriteReg(nDev_ID, FAULT_SUM, 1, 1, FRMWRT_SGL_R);					// send read stored values command
	nRead = WaitRespFrame(bFrame, 20, 4);									// 32 bytes data + packet header + CRC, 0ms timeout
	for (int i=1;i<=7;i++)
	{
		*ptr_bf++ = (bFrame[i*2-1]<<8) | bFrame[i*2] ;  // bFrame[1] << 8 | bFrame[2]
	}
	*ptr_bf++ = bFrame[15] ;
	*ptr_bf = (bFrame[16]<<8) | bFrame[17] ;  // bFrame[1] << 8 | bFrame[2]
	read_faults();	fault_output();
}

void read_faults()
{
	
	nRead = ReadReg(nDev_ID, FAULT_SUM, &FLT_summ, 2, 0);				// Fault summary
	//user_debug("Fault summary: %d\r\n",FLT_summ);						// Read fault summary
	
	nRead = ReadReg(nDev_ID, FAULT_UV, &FLT_UV, 2, 0);					// Fault_UV
	//user_debug("Under voltage Fault : %d\r\n",FLT_UV);					// Read under voltage faults
	
	nRead = ReadReg(nDev_ID, FAULT_OV, &FLT_OV, 2, 0);					// Fault_OV
	//user_debug("Over voltage Fault : %d\r\n",FLT_OV);					// Read Over voltage faults
	
	nRead = ReadReg(nDev_ID, FAULT_AUX, &FLT_AUX, 2, 0);				// Fault_Auxillary thermistors
	//user_debug("Auxilary fault: %d\r\n",FLT_AUX);
	
	nRead = ReadReg(nDev_ID, FAULT_2UV, &FLT_COMUV, 2, 0);				// Fault_Comparator undervoltage
	//user_debug("Comparator under voltage Fault : %d\r\n",FLT_COMUV);	// Read comp under voltage faults

	nRead = ReadReg(nDev_ID, FAULT_2OV, &FLT_COMOV, 2, 0);				// Fault_Comparator Overvoltage
	//user_debug("Comparator over voltage Fault : %d\r\n",FLT_COMOV);		// Read Comp over voltage fault
	
	nRead = ReadReg(nDev_ID, FAULT_COM, &FLT_COMM, 2, 0);				// Fault_Communication
	//user_debug("Communication Fault : %d\r\n",FLT_COMM);				// Read Communication fault
	
	nRead = ReadReg(nDev_ID, FAULT_SYS, &FLT_SYS,1,0);					// System fault
	//user_debug("System faults: %d\r\n",FLT_SYS);
	
	nRead = ReadReg(nDev_ID, FAULT_DEV, &FLT_CHIP, 2, 0);				// Chip fault
	//user_debug("CHIP faults = %d\r\n", FLT_CHIP);
	
	num_FLT_OV = count_faultcells(FLT_OV);
	num_FLT_UV = count_faultcells(FLT_UV);
	num_FLT_AUX = count_faultcells(FLT_AUX);
	
	user_debug("Number of OV fault cells: %d \r\n", num_FLT_OV);
	user_debug("Number of UV fault cells: %d \r\n", num_FLT_UV);
	user_debug("Number of AUX fault Thermisters : %d \r\n", num_FLT_AUX);
	
	if(num_FLT_AUX > 0)
	{
		user_debug("AUX fault has occured\r\n");
		user_debug("Action required:Turn off Charging, Turn off Discharging\r\n");
		Discharge_OFF();
		Charging_OFF();
	}
	if(num_FLT_OV > 0)
	{
		Charging_OFF();
		user_debug("OV fault has occured\r\n");
		user_debug("Action required:Turn off Charging\r\n");
		//user_debug("Action required:Turn off Charging, Turn on Discharging\r\n");
		//Discharge_ON();
	}
	if(num_FLT_UV > 0)
	{
		Discharge_OFF();
		user_debug("UV fault has occured\r\n");
		user_debug("Action required:Turn off Discharging\r\n");
		//user_debug("Action required:Turn on Charging, Turn off Discharging\r\n");
		//Charging_ON();
	}
	//if(num_FLT_OV == 0 && num_FLT_UV == 0 && num_FLT_AUX == 0 )
	//{
		//user_debug("no fault\r\n");
		//user_debug("Turn on Charging, Turn on Discharging\r\n");
		//Charging_ON();
		//Discharge_ON();
	//}
	
}

uint16_t count_faultcells(int flt_byte)
{
	uint16_t decimal_num, remainder, base = 1, binary = 0, no_of_1s = 0;
	//flt_byte = FLT_OV;                        // edited FLT_OV to FLT_UV 31-12-2018
	while (flt_byte > 0)
	{
		remainder = flt_byte % 2;
		//  To count no.of 1s
		if (remainder == 1)
		{
			no_of_1s++;
		}
		binary = binary + remainder * base;
		flt_byte = flt_byte / 2;
		base = base * 10;
	}
	return no_of_1s;
	
}

/*
uint8_t charge_on_scenario, discharge_on_scenario;
switch(charge_on_scenario)
{
case 1:
// cooling ON
// OT_Timer ON
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
case 2:
Charging_OFF();
break;
case 3:
Charging_OFF;
// cooling ON
// OT_Timer ON
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
case 4:
if (num_FLT_OV > 6)
{
Balance_ON(FLT_OV);
Charging_OFF;
break;
}
else
{
Balance_ON(FLT_OV);
break;
}
case 5:
if (num_FLT_OV > 6)
{
Balance_ON(FLT_OV);
Charging_OFF;
// coolingON
// OT timerON
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
}
else
{
Balance_ON(FLT_OV);
// coolingON
// OT timerON
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}

}
case 6:
Balance_ON(FLT_OV);
Charging_OFF();
break;
case 7:
Balance_ON(FLT_OV);
Charging_OFF;
// coolingON
// OT timerON
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
default:
Charging_ON();
Balance_OFF();
// cooling OFF
// OT Timer OFF
break;
}

switch(discharge_on_scenario)
{
case 1:
// cooling on
// OT timer on
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
case 2:
// OC timer on 10sec
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}
case 3:
// cooling on
// OC timer on
// OT timer on
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}

if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
case 4:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
break;
}
case 5:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
// cooling on
// OT timer on
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}

}
case 6:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
// OC timer on 10sec
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}
}
case 7:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
// cooling on
// OC timer on
// OT timer on
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}

if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
}
case 8:
// warning on
// power reduction
break;
case 9:
// warning on
// power reduction
// cooling on
// OT timer on
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
case 10:
// warning on
// power reduction
// OC timer on 10sec
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}
case 11:
// warning on
// power reduction
// cooling on
// OC timer on
// OT timer on
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}

if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
case 12:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
break;
}
case 13:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
// cooling on
// OT timer on
if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}

}
case 14:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
// OC timer on 10sec
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}
}
case 15:
if (soc < 20)
{
Discharge_OFF();
break;
}
else
{
// warning on
// power reduction
// cooling on
// OC timer on
// OT timer on
if (overcurrent_flt)
{
Discharge_OFF();
// hysteresis
break;
}
else
{
// Go to main
break;
}

if (FLT_AUX != 0)
{
Charging_OFF();
Discharge_OFF();
break;
}
else
{
// Go to main
break;
}
}
default:
Discharge_ON();
// warning OFF
// power OK
// OC timer off
// OT timer off
// cooling off
}
*/

void sci_user_write(uint8_t *data,uint16_t usr_len)
{
	#ifdef CONF_SCI
	uint16_t length=usr_len;
	SercomUsart *const usart_hw = (SercomUsart *)SCI_MODULE;
	while(length--)
	{
		uint8_t data_to_send = *data;
		while(!((usart_hw->INTFLAG.reg)&SERCOM_USART_INTFLAG_DRE));
		usart_hw->DATA.reg = (data_to_send & SERCOM_USART_DATA_MASK);
		data++;
	}
	#endif
}

void sci_user_read(uint8_t *data, uint16_t usr_len)
{
	uint16_t length = usr_len;
	uint16_t ret = 0;
	ret = usart_read_buffer_wait(&sci_uart_module,data,length);
	if(ret == STATUS_OK)
	{
		for(int i=0;i<length;i++)
		{
			//user_debug("%d: %02x\r\n",i,data[i]);
		}
	}
	else
	{
		user_debug("SCI UART RX Status %d\r\n",ret);
	}
}

int WriteReg(uint16_t bID, uint16_t wAddr, uint64_t dwData, uint16_t bLen, uint16_t bWriteType)
{
	int bRes = 0;
	char bBuf[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	switch(bLen)
	{
		case 1:
		bBuf[0] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 1, bWriteType);
		break;
		case 2:
		bBuf[0] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[1] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 2, bWriteType);
		break;
		case 3:
		bBuf[0] = (dwData & 0x0000000000FF0000) >> 16;
		bBuf[1] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[2] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 3, bWriteType);
		break;
		case 4:
		bBuf[0] = (dwData & 0x00000000FF000000) >> 24;
		bBuf[1] = (dwData & 0x0000000000FF0000) >> 16;
		bBuf[2] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[3] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 4, bWriteType);
		break;
		case 5:
		bBuf[0] = (dwData & 0x000000FF00000000) >> 32;
		bBuf[1] = (dwData & 0x00000000FF000000) >> 24;
		bBuf[2] = (dwData & 0x0000000000FF0000) >> 16;
		bBuf[3] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[4] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 5, bWriteType);
		break;
		case 6:
		bBuf[0] = (dwData & 0x0000FF0000000000) >> 40;
		bBuf[1] = (dwData & 0x000000FF00000000) >> 32;
		bBuf[2] = (dwData & 0x00000000FF000000) >> 24;
		bBuf[3] = (dwData & 0x0000000000FF0000) >> 16;
		bBuf[4] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[5] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 6, bWriteType);
		break;
		case 7:
		bBuf[0] = (dwData & 0x00FF000000000000) >> 48;
		bBuf[1] = (dwData & 0x0000FF0000000000) >> 40;
		bBuf[2] = (dwData & 0x000000FF00000000) >> 32;
		bBuf[3] = (dwData & 0x00000000FF000000) >> 24;
		bBuf[4] = (dwData & 0x0000000000FF0000) >> 16;
		bBuf[5] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[6] =  dwData & 0x00000000000000FF;;
		bRes = WriteFrame(bID, wAddr, bBuf, 7, bWriteType);
		break;
		case 8:
		bBuf[0] = (dwData & 0xFF00000000000000) >> 56;
		bBuf[1] = (dwData & 0x00FF000000000000) >> 48;
		bBuf[2] = (dwData & 0x0000FF0000000000) >> 40;
		bBuf[3] = (dwData & 0x000000FF00000000) >> 32;
		bBuf[4] = (dwData & 0x00000000FF000000) >> 24;
		bBuf[5] = (dwData & 0x0000000000FF0000) >> 16;
		bBuf[6] = (dwData & 0x000000000000FF00) >> 8;
		bBuf[7] =  dwData & 0x00000000000000FF;
		bRes = WriteFrame(bID, wAddr, bBuf, 8, bWriteType);
		break;
		default:
		break;
	}
	return bRes;
}

int WriteFrame(uint16_t bID, uint16_t wAddr, char * pData, uint16_t bLen, uint16_t bWriteType)
{
	int    bPktLen = 0;
	char   pFrame[32];
	char * pBuf = pFrame;
	uint16_t   wCRC;

	if (bLen == 7 || bLen > 8)
	return 0;

	memset(pFrame, 0x7F, sizeof(pFrame));
	if (wAddr > 255)    {
		*pBuf++ = 0x88 | bWriteType | bLen; // use 16-bit address
		if (bWriteType == FRMWRT_SGL_R || bWriteType == FRMWRT_SGL_NR || bWriteType == FRMWRT_GRP_R || bWriteType == FRMWRT_GRP_NR)//(bWriteType != FRMWRT_ALL_NR)// || (bWriteType != FRMWRT_ALL_R))
		{
			*pBuf++ = (bID & 0x00FF);
		}
		*pBuf++ = (wAddr & 0xFF00) >> 8;
		*pBuf++ =  wAddr & 0x00FF;
	}
	else {
		*pBuf++ = 0x80 | bWriteType | bLen; // use 8-bit address
		if (bWriteType == FRMWRT_SGL_R || bWriteType == FRMWRT_SGL_NR || bWriteType == FRMWRT_GRP_R || bWriteType == FRMWRT_GRP_NR)
		{
			*pBuf++ = (bID & 0x00FF);
		}
		*pBuf++ = wAddr & 0x00FF;
	}

	while(bLen--)
	*pBuf++ = *pData++;

	bPktLen = pBuf - pFrame;

	wCRC = CRC16(pFrame, bPktLen);
	*pBuf++ = wCRC & 0x00FF;
	*pBuf++ = (wCRC & 0xFF00) >> 8;
	bPktLen += 2;
	
	sci_user_write(pFrame, bPktLen);

	return bPktLen;
}

int ReadReg(uint16_t bID, uint16_t wAddr, void * pData, uint16_t bLen, uint32_t dwTimeOut)
{
	int   bRes = 0;
	char  bRX[8];

	memset(bRX, 0, sizeof(bRX));
	switch(bLen)
	{
		case 1:
		bRes = ReadFrameReq(bID, wAddr, 1);
		bRes = WaitRespFrame(bRX, 4, dwTimeOut);
		if (bRes == 1)
		*((char *)pData) = bRX[1] & 0x00FF;
		else
		bRes = 0;
		break;
		case 2:
		bRes = ReadFrameReq(bID, wAddr, 2);
		bRes = WaitRespFrame(bRX, 5, dwTimeOut);
		if (bRes == 2)
		*((uint16_t *)pData) = ((uint16_t)bRX[1] << 8) | (bRX[2] & 0x00FF);
		else
		bRes = 0;
		break;
		case 3:
		bRes = ReadFrameReq(bID, wAddr, 3);
		bRes = WaitRespFrame(bRX, 6, dwTimeOut);
		if (bRes == 3)
		*((uint32_t *)pData) = ((uint32_t)bRX[1] << 16) | ((uint16_t)bRX[2] << 8) | (bRX[3] & 0x00FF);
		else
		bRes = 0;
		break;
		case 4:
		bRes = ReadFrameReq(bID, wAddr, 4);
		bRes = WaitRespFrame(bRX, 7, dwTimeOut);
		if (bRes == 4)
		*((uint32_t *)pData) = ((uint32_t)bRX[1] << 24) | ((uint32_t)bRX[2] << 16) | ((uint16_t)bRX[3] << 8) | (bRX[4] & 0x00FF);
		else
		bRes = 0;
		break;
		default:
		break;
	}
	return bRes;
}

int ReadFrameReq(uint16_t bID, uint16_t wAddr, char bByteToReturn)
{
	char bReturn = bByteToReturn - 1;

	if (bReturn > 127)
	return 0;

	return WriteFrame(bID, wAddr, &bReturn, 1, FRMWRT_SGL_R);
}

int WaitRespFrame(char *pFrame, uint16_t bLen, uint32_t dwTimeOut)
{
	uint16_t wCRC = 0, wCRC16;
	char bBuf[132];
	char bRxDataLen;

	memset(bBuf, 0, sizeof(bBuf));
	//user_debug("Expecting Length %d\r\n", bLen);

	sci_user_read(bBuf, bLen);
	//while(UART_RX_RDY == 0U)
	//{
	//// Check for timeout.
	//if(RTI_TIMEOUT == 1U)
	//{
	//RTI_TIMEOUT = 0;
	//return 0; // timed out
	//}
	//} /* Wait */
	
	//UART_RX_RDY = 0;
	bRxDataLen = bBuf[0];

	delay_ms(dwTimeOut);

	// rebuild bBuf to have bLen as first byte to use the same CRC function as TX
	/*  i = bRxDataLen + 3;
	while(--i >= 0)
	{
	bBuf[i + 1] = bBuf[i];
	}
	bBuf[0] = bRxDataLen;
	*/
	wCRC = bBuf[bRxDataLen+2];
	wCRC |= ((uint16_t)bBuf[bRxDataLen+3] << 8);
	wCRC16 = CRC16(bBuf, bRxDataLen+2);
	if (wCRC != wCRC16)
	return -1;

	memcpy(pFrame, bBuf, bRxDataLen + 4);

	return bRxDataLen + 1;
}


// CRC16 for PL455
// ITU_T polynomial: x^16 + x^15 + x^2 + 1
const uint16_t crc16_table[256] = {
	0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
	0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
	0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
	0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
	0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
	0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
	0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
	0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
	0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
	0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
	0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
	0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
	0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
	0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
	0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
	0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
	0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
	0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
	0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
	0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
	0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
	0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
	0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
	0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
	0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
	0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
	0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
	0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
	0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
	0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
	0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
	0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};


uint16_t CRC16(char *pBuf, int nLen)
{
	uint16_t wCRC = 0;
	int i;

	for (i = 0; i < nLen; i++)
	{
		wCRC ^= (*pBuf++) & 0x00FF;
		wCRC = crc16_table[wCRC & 0x00FF] ^ (wCRC >> 8);
	}

	return wCRC;
}

// Big endian / Little endian conversion
uint16_t  B2SWORD(uint16_t wIN)
{
	uint16_t wOUT = 0;

	wOUT =   wIN & 0x00FF >> 8;
	wOUT |= (wIN & 0xFF00) >> 8;

	return wOUT;
}

uint32_t B2SDWORD(uint32_t dwIN)
{
	uint32_t dwOUT = 0;

	dwOUT =   dwIN & 0x00FF >> 8;
	dwOUT |= (dwIN & 0xFF00) >> 8;
	dwOUT |=  dwIN & 0x00FF >> 8;
	dwOUT |= (dwIN & 0xFF00) >> 8;

	return dwOUT;
}

uint32_t B2SINT24(uint32_t dwIN24)
{
	uint32_t dwOUT = 0;

	dwOUT =   dwIN24 & 0x00FF >> 8;
	dwOUT |= (dwIN24 & 0xFF00) >> 8;
	dwOUT |=  dwIN24 & 0x00FF >> 8;

	return dwOUT;
}


