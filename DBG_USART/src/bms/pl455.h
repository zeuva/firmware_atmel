/*
 *  @file pl455.h
 *
 *  @author Stephen Holland - Texas Instruments Inc.
 *  @date June 2015
 *  @version 1.0 Initial version
 *  @note Built with CCS for Hercules Version: 5.5.0
 */

/*****************************************************************************
**
**  Copyright (c) 2011-2015 Texas Instruments
**
******************************************************************************/


#ifndef PL455_H_
#define PL455_H_

#include <asf.h>

typedef struct {
	uint16_t cell_volt1;
	uint16_t cell_volt2;
	uint16_t cell_volt3;
	uint16_t cell_volt4;
	uint16_t cell_volt5;
	uint16_t cell_volt6;
	uint16_t cell_volt7;
	uint16_t cell_volt8;
	uint16_t cell_volt9;
	uint16_t cell_volt10;
	uint16_t cell_volt11;
	uint16_t cell_volt12;
	uint16_t cell_volt13;
	uint16_t cell_volt14;
	uint16_t cell_volt15;
	uint16_t cell_volt16;
}CELLVOLT;

typedef struct {
	uint16_t cell_temp1;
	uint16_t cell_temp2;
	uint16_t cell_temp3;
	uint16_t cell_temp4;
	uint16_t cell_temp5;
	uint16_t cell_temp6;
	uint16_t cell_temp7;
	uint16_t cell_temp8;
}CELLTEMP;

typedef struct {
	uint16_t fault_sum;
	uint16_t fault_uv;
	uint16_t fault_ov;
	uint16_t fault_aux;
	uint16_t fault_2uv;
	uint16_t fault_2ov;
	uint16_t cell_com;
	uint16_t cell_sys;
	uint16_t cell_dev;
	uint16_t cell_gpi;
}BMSFAULT;

typedef struct {
	CELLVOLT bms_cv;
	CELLTEMP bms_ct;
	BMSFAULT bms_bf;
}BMS_DATA;


// User defines
#define FRMWRT_SGL_R	0x00 // single device write with response
#define FRMWRT_SGL_NR	0x10 // single device write without response
#define FRMWRT_GRP_R	0x20 // group broadcast with response
#define FRMWRT_GRP_NR	0x30 // group broadcast without response
#define FRMWRT_ALL_R	0x60 // general broadcast with response
#define FRMWRT_ALL_NR	0x70 // general broadcast without response

#define TOTALBOARDS	1
#define SCI_BAUDRATE 250000

// Fault Commands
#define FAULT_SUM 82	// FAULT SUMMARY
#define FAULT_UV  84	// UNDERVOLTAGE FAULTS
#define FAULT_OV  86	// OVERVOLTAGE FAULTS
#define FAULT_AUX 88	// AUX THRESHOLD EXCEEDED FAULTS
#define FAULT_2UV 90	// COMPARATOR UV FAULT
#define FAULT_2OV 92	// COMPARATOR OV FAULT
#define FAULT_COM 94	// COMMUNICATION FAULT
#define FAULT_SYS 96	// SYSTEM FAULT
#define FAULT_DEV 97	// DEVICE FAULT
#define FAULT_GPI 99	// GPIO FAULT
#define MASK_COMM 104	// COMMUNICATIONS FAULT MASK REGISTER
#define MASK_SYS  106	// SYSTEM FAULT MASK REGISTER
#define MASK_DEV  107	// CHIP FAULT MASK REGISTER
#define FO_CTRL	  110	// FAULT OUTPUT CONTROL
#define CSUM	  240	// SAVED CHECKSUM VALUE
#define CSUM_RSLT 244	// CHECKSUM READOUT

// Threshold Commands
#define COMP_UV	  140	// COMPARATOR UNDERVOLTAGE THRESHOLD
#define COMP_OV	  141	// COMPARATOR OVER VOLTAGE THRESHOLD
#define CELL_UV	  142	// CELL UNDERVOLTAGE THRESHOLD
#define CELL_OV	  144	// CELL OVER VOLTAGE THRESHOLD
#define AUX0_UV	  146	// AUX0 UV THRESHOLD
#define AUX0_OV	  148	// AUX0 OV THRESHOLD
#define AUX1_UV	  150	// AUX1 UV THRESHOLD
#define AUX1_OV	  152	// AUX1 OV THRESHOLD
#define AUX2_UV	  154	// AUX2 UV THRESHOLD
#define AUX2_OV	  156	// AUX2 OV THRESHOLD
#define AUX3_UV	  158	// AUX3 UV THRESHOLD
#define AUX3_OV	  160	// AUX3 OV THRESHOLD
#define AUX4_UV	  162	// AUX4 UV THRESHOLD
#define AUX4_OV	  164	// AUX4 OV THRESHOLD
#define AUX5_UV	  166	// AUX5 UV THRESHOLD
#define AUX5_OV	  168	// AUX5 OV THRESHOLD
#define AUX6_UV	  170	// AUX6 UV THRESHOLD
#define AUX6_OV	  172	// AUX6 OV THRESHOLD
#define AUX7_UV	  174	// AUX7 UV THRESHOLD
#define AUX7_OV	  176	// AUX7 OV THRESHOLD

// Configuration commands
#define CMD		  2		// COMMAND
#define CHANNELS  3		// COMMAND CHANNEL SELECT
#define OVERSMPL  7		// COMMAND AVERAGING (OVERSAMPLING)
#define ADDR	  10	// DEVICE ADDRESS
#define GROUP_ID  11	// (DEVICE) GROUP IDENTIFIER
#define DEV_CTRL  12	// DEVICE CONTROL
#define NCHAN	  13	// NUMBER OF CHANNELS ENABLED FOR CONVERSION
#define DEVCONFIG 14	// DEVICE CONFIGURATION
#define PWRCONFIG 15	// POWER CONFIGURATION
#define COMCONFIG 16	// COMMUNICATION CONFIGURATION
#define TXHOLDOFF 18	// UART TRANSMITTER HOLDOFF
#define CBCONFIG  19	// CELL BALANCING CONFIGURATION
#define CBENBL	  20	// CELL BALANCING ENABLES
#define AM_PER	  50	// AUTO MONITOR PERIOD
#define AM_CHAN	  51	// AUTO-MONITOR CHANNEL SELECT
#define AM_OSMPL  55	// AUTO-MONITOR AVERAGING
#define SMPL_DLY1 61	// INITIAL SAMPLING DELAY
#define CELL_SPER 62	// CELL & DIE TEMPERATURE MEASUREMENT PERIOD
#define AUX_SPER  63	// AUX CHANNELS SAMPLING PERIOD
#define DEV_STATUS	  81	// DEVICE STATUS
#define SHDN_STS  80	// SHUTDOWN RECOVERY STATUS
#define LOT_NUM   190	// DEVICE LOT NUMBER
#define SER_NUM   198	// DEVICE SERIAL NUMBER

// OFFSET CORRECTION COMMANDS
#define VSOFFSET  210	// ADC VOLTAGE OFFSET CORRECTION
#define VSGAIN	  211	// ADC VOLTAGE GAIN CORRECTION
#define AX0OFFSET 212	//	AUX0 ADC OFFSET CORRECTION 
#define AX1OFFSET 214	//	AUX1 ADC OFFSET CORRECTION
#define AX2OFFSET 216	//	AUX2 ADC OFFSET CORRECTION
#define AX3OFFSET 218	//	AUX3 ADC OFFSET CORRECTION
#define AX4OFFSET 220	//	AUX4 ADC OFFSET CORRECTION
#define AX5OFFSET 222	//	AUX5 ADC OFFSET CORRECTION
#define AX6OFFSET 224	//	AUX6 ADC OFFSET CORRECTION
#define AX7OFFSET 226	//	AUX7 ADC OFFSET CORRECTION

struct usart_module sci_uart_module;

void WakePL455(void);
//void sci_rx_handler(uint8_t instance);
void pl455_port_init();
void configure_SCI_uart();
void Charging_ON();
void Charging_OFF();
void Discharge_ON();
void Discharge_OFF();
void Overcharge();
void OverDischarge();
void Balance_ON(uint16_t ovf);
void Max_cell_Bal_ON();
void Balance_OFF();
void Overcurrent();
void bq76pl455_init();
void mask_checksum_fault();
void uart_comm();
void read_tx_holdoff();
void write_tx_holdoff();
void serial_num();
void channel_sel();
void power_config();
void device_config();
void fault_output();
void Voltage_threshold();
void cellVoltmeasurement();
void readvoltage();
void comconfig();
void Read_debug(uint16_t deb);
uint16_t count_faultcells();
void Thermistor_threshold();
void read_faults();
void read_all_faults();
void checksum_write();
void device_control();
void device_add();
void configure_AFE();
void scenarios();
void sci_user_write(uint8_t *data,uint16_t usr_len);
void sci_user_read(uint8_t *data, uint16_t usr_len);
int WriteReg(uint16_t bID, uint16_t wAddr, uint64_t dwData, uint16_t bLen, uint16_t bWriteType);
int WriteFrame(uint16_t bID, uint16_t wAddr, char * pData, uint16_t bLen, uint16_t bWriteType);
int ReadReg(uint16_t bID, uint16_t wAddr, void * pData, uint16_t bLen, uint32_t dwTimeOut);
int ReadFrameReq(uint16_t bID, uint16_t wAddr, char bByteToReturn);
int WaitRespFrame(char *pFrame, uint16_t bLen, uint32_t dwTimeOut);
uint16_t CRC16(char *pBuf, int nLen);
uint16_t  B2SWORD(uint16_t wIN);
uint32_t B2SDWORD(uint32_t dwIN);
uint32_t B2SINT24(uint32_t dwIN24);


#endif /* PL455_H_ */
//EOF
