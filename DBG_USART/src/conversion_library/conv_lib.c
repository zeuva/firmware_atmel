/*
 * conv_lib.c
 *
 * Created: 28/01/2018 3:36:01 PM
 *  Author: Arjun
 */ 

#include <conversion_library/conv_lib.h>
/*********************************************************************************************************************************/
/*
*	Host to Network order (Little Endian to Big Endian)
*	Parameter: Value in Little Endian format
*	Return:	Returns value in Big Endian Format.
*/
/*********************************************************************************************************************************/
void htonl(uint16_t *n)
{
	uint16_t temp;
	temp = (*n & 0x000ff) << 8;
	temp = temp | ((*n & 0xff00) >> 8);
	*n = temp;
}

uint16_t htonl_wp(uint16_t n)
{
	uint16_t temp;
	temp = (n & 0x000ff) << 8;
	temp = temp | ((n & 0xff00) >> 8);
	return temp;
}

uint32_t htonl32_wp(uint32_t n)
{
	uint32_t temp;
	temp = (n & 0x000000ff) << 24;
	temp = temp | ((n & 0x0000ff00) << 8);
	temp = temp | ((n & 0x00ff0000) >> 8);
	temp = temp | ((n & 0xff000000) >> 24);
	return temp;
}

/*********************************************************************************************************************************/
/*
*	Ascii TO Equivalent Hex
*	Parameter_1: Character Array which contains the Ascii Value
*	Parameter_2: Number of Ascii Values in the Array need to be Converted to Hex
*	Return:		 Equivalent Hex Value
*/
/*********************************************************************************************************************************/
uint64_t atohex(char *buffer,uint8_t counter)
{
	uint64_t value=0;
	uint8_t i=0;
	while(counter--)
	{
		if(counter>0)
		{

			if(buffer[i]>0x2f && buffer[i]<0x3a)
			{
				value= value +(buffer[i] & 0x0f);
			}
			
			else if(buffer[i]>0x40 && buffer[i]<0x47)
			{
				value= value + (buffer[i]-0x37);
			}
			else if(buffer[i]>0x60 && buffer[i]<0x67)
			{
				value= value+ (buffer[i]-0x57);
			}
			else
			{
			}
			if(counter>1)
			{
				value=value<<4;
			}
			
		}
		i++;
	}
	
	return value;
}


/*********************************************************************************************************************************/
/*
*	Ascii TO Equivalent Decimal
*	Parameter_1: Character Array which contains the Ascii Value
*	Parameter_2: Number of Ascii Values in the Array need to be Converted to Decimal
*	Return:		 Equivalent Decimal Value
*/
/*********************************************************************************************************************************/
uint64_t atodec(char *buffer,uint8_t counter)
{
	uint64_t volatile value=0;
	uint8_t i=0;
	while(counter--)
	{
		
		if(buffer[i]>0x2f && buffer[i]<0x3a)
		{
			value= value*10 + (buffer[i] & 0x0f);
		}
		i++;
	}

	return value;
}

/*********************************************************************************************************************************/
/*
*	Hex TO Equivalent Ascii
*	Parameter_1: Array which contains Hex value
*	Parameter_2: Array in which Hex Equivalent Ascii will be stored
*	Parameter_3: Array length
*/
/*********************************************************************************************************************************/
void hex_to_ascii(uint8_t *buffer_in,uint8_t *buffer_out,uint8_t length)
{
	int j=0;
	for(int i=0;i<length;i++)
	{	// Hex Value is seperated into two nibbles & stores in separate Location
		buffer_out[j++]= ((buffer_in[i] & 0xf0)>>4);
		buffer_out[j++]= (buffer_in[i] & 0x0f);
	}
	for(int i=0;i<(length*2);i++)
	{
		if(buffer_out[i]<0x0a)
		{
			// If the value is less than 10, then ADD with 0x30
			buffer_out[i]=buffer_out[i]+0x30;
		}
		else
		{
			//Else ADD with 0x37
			buffer_out[i]=buffer_out[i]+0x37;
		}
	}
}


void hex_to_binary(uint8_t *buffer_in,uint8_t *buffer_out,uint8_t length)
{
	uint8_t logic_shift;
	for(int i=0;i<length;i++)
	{
		logic_shift=0x80;
		for(int j=0;j<8;j++)
		{
			if(buffer_in[i]&logic_shift)
			{
				buffer_out[(i*16)+(j*2)+0]='1';
				buffer_out[(i*16)+(j*2)+1]=',';
			}
			else
			{
				buffer_out[(i*16)+(j*2)+0]='0';
				buffer_out[(i*16)+(j*2)+1]=',';
			}
			logic_shift=logic_shift>>1;
		}
	}
}

/**********************************************************************************************************************************
*	HEX to DEC conversion
*	Parameter:	32 bit hex value to be converted to decimal
*	Return:		Returns a 32 bit hex equivalent of decimal value.
**********************************************************************************************************************************/
uint32_t dec_to_hex(uint32_t data)
{
	uint32_t value;
	value = data/10000;
	data = data%10000;
	value = value<<4;
	value = value +(data/1000);
	data = data%1000;
	value = value<<4;
	value = value + (data/100);
	data = data%100;
	value = value<<4;
	value = value +(data/10);
	value = value<<4;
	value = value + (data % 10);
	return value;
}


uint16_t hex_to_dec(uint16_t hexval,uint8_t varsize)
{
	uint16_t ret_val=0;
	uint16_t multiplier=1;
	for(int i=0;i<(varsize*2);i++)
	{
		for(int j=0;j<i;j++)
		{
			multiplier=multiplier*10;
		}
		ret_val=ret_val + ((hexval&0x0f)*multiplier);
		hexval=hexval>>0x04;
	}
	return ret_val;
}

/*********************************************************************************************************************************/
/*
*	Network to Host order (Big Endian to Little Endian)
	Parameter: Value in Big Endian format
	Return:	Returns value in Little Endian Format.                                                                      
*/
/*********************************************************************************************************************************/
uint16_t ntohl(uint16_t n)
{
	uint16_t temp;
	temp = (n & 0x000ff) << 8;
	temp = temp | ((n & 0xff00) >> 8);
	return temp;
}

int32_t ntohl_neg(uint16_t n,uint8_t sign)
{
	int32_t temp;
	temp = (n & 0x000ff) << 8;
	temp = temp | ((n & 0xff00) >> 8);
	if(sign==0xff)
	{
		temp *=-1;
	}
	return temp;
}



uint32_t ntohl_32(uint32_t n)
{
	uint32_t temp;
	temp = (n & 0x000000ff) <<24;
	temp = temp | ((n & 0x0000ff00) <<8);
	temp = temp | ((n & 0x00ff0000) >>8);
	temp = temp | ((n & 0xff000000) >>24);
	return temp;
}

/**********************************************************************************************************************************
*	HEX to DEC conversion
*	Parameter:	32 bit hex value to be converted to decimal
*	Return:		Returns a 32 bit hex equivalent of decimal value.
**********************************************************************************************************************************/
uint16_t dec_to_hex8bit(uint8_t data)
{
	uint16_t value;
	value = value + (data/100);
	data = data%100;
	value = value<<4;
	value = value +(data/10);
	value = value<<4;
	value = value + (data % 10);
	return value;
}

