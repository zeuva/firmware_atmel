/*
 * conv_lib.h
 *
 * Created: 28/01/2018 3:35:49 PM
 *  Author: Arjun
 */ 


#ifndef CONV_LIB_H_
#define CONV_LIB_H_

#include <asf.h>

void htonl(uint16_t *n);
uint16_t htonl_wp(uint16_t n);
uint32_t htonl32_wp(uint32_t n);

uint16_t ntohl(uint16_t n);
int32_t ntohl_neg(uint16_t n,uint8_t sign);
uint32_t ntohl_32(uint32_t n);

uint64_t atohex(char *buffer,uint8_t counter);
uint64_t atodec(char *buffer,uint8_t counter);
void hex_to_ascii(uint8_t *buffer_in,uint8_t *buffer_out,uint8_t length);
uint32_t dec_to_hex(uint32_t data);
uint16_t dec_to_hex8bit(uint8_t data);
uint16_t hex_to_dec(uint16_t hexval,uint8_t varsize);
void hex_to_binary(uint8_t *buffer_in,uint8_t *buffer_out,uint8_t length);




#endif /* CONV_LIB_H_ */