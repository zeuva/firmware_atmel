/*
 * debug.c
 *
 * Created: 11/25/2017 10:26:01 AM
 *  Author: Owner
 */ 

#include "debug.h"

int debug_uart_init()
{
	//! [setup_config]
	struct usart_config config_usart;
	//! [setup_config]
	//! [setup_config_defaults]
	usart_get_config_defaults(&config_usart);
	//! [setup_config_defaults]

	config_usart.baudrate = DBG_UART_BAUDRATE;
	config_usart.character_size = DBG_UART_CHAR_LENGTH;
	config_usart.parity = DBG_UART_PARITY;
	config_usart.stopbits = DBG_UART_STOP_BITS;
	
	config_usart.mux_setting = USART_RX_1_TX_0_XCK_1;
	config_usart.pinmux_pad0 = DBG_TX;
	config_usart.pinmux_pad1 = DBG_RX;
	config_usart.pinmux_pad2 = PINMUX_UNUSED;
	config_usart.pinmux_pad3 = PINMUX_UNUSED;
	//! [setup_change_config]

	//! [setup_set_config]
	stdio_serial_init(&dbg_usart, DBG_MODULE, &config_usart);
	//! [setup_set_config]

	//! [setup_enable];
	usart_enable(&dbg_usart);
	//! [setup_enable]
	
	return 1;
	
}