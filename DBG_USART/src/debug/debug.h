/*
 * debug.h
 *
 * Created: 11/25/2017 10:25:26 AM
 *  Author: Owner
 */ 


#ifndef DEBUG_H_
#define DEBUG_H_

#include <asf.h>

struct usart_module dbg_usart;

#define DBG_UART_BAUDRATE 115200;
#define DBG_UART_CHAR_LENGTH USART_CHARACTER_SIZE_8BIT
#define DBG_UART_PARITY  USART_PARITY_NONE
#define DBG_UART_STOP_BITS USART_STOPBITS_1

int debug_uart_init(void);

#endif /* DEBUG_H_ */