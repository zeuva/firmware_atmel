/*
 * Flash_Api.c
 *
 * Created: 06/01/2018 12:31:49 PM
 *  Author: Arjun
 */ 

#include "flash/flash_phy.h"
#include "flash/Flash_Api.h"
#include <Internal_eeprom/emulated_eeprom.h>

static uint32_t flash_data_size;
static uint32_t flash_write_address;
static uint32_t flash_read_address; 
uint32_t flash_webclient_read_address;
uint32_t flash_data_erase;
uint32_t data_packet_size;

static uint32_t Flash_data_lst_addr;


void flash_init(void)
{
	#ifdef CONF_FLASH
		#ifdef CONF_SPI
			configure_flash_spi();
			spi_flash_init();
		#endif
	#endif
}

void flash_address_init(void)
{
	uint8_t flash_error = 0;
	backup_global_variable(&flash_write_address,10,sizeof(flash_write_address),EEPROM_READ);
	backup_global_variable(&flash_read_address,20,sizeof(flash_read_address),EEPROM_READ);
	backup_global_variable(&flash_webclient_read_address,30,sizeof(flash_webclient_read_address),EEPROM_READ);
	backup_global_variable(&flash_data_erase,40,sizeof(flash_data_erase),EEPROM_READ);

	Flash_data_lst_addr = (((((ONBOARD_FLASH+1)*3)/4)-4096)/sizeof(GLOBAL_PKT))*sizeof(GLOBAL_PKT);
	flash_data_size= Flash_data_lst_addr;
	if(flash_write_address>ERROR_FLASH_ADDRESS)
	{
		flash_write_address=0;
		flash_error = 1;
	}
	if(flash_webclient_read_address>ERROR_FLASH_ADDRESS)
	{
		flash_webclient_read_address=0;
		flash_error = 1;
	}
	if(flash_read_address>ERROR_FLASH_ADDRESS)
	{
		flash_read_address=0;
		flash_error = 1;
	}
	if(flash_data_erase>ERROR_FLASH_ADDRESS)
	{
		flash_data_erase=0;
		flash_error = 1;
	}
	if(flash_webclient_read_address>flash_write_address)
	{
		flash_write_address=0;
		flash_webclient_read_address=0;
		flash_error = 1;
	}
	if(flash_error>1)
	{
		flash_clear_memory(ERASE_ALL,0);		
	}
}

void flash_buffer_write(uint8_t *wr_ptr, uint16_t wr_sz)
{
	uint32_t loc_fwa = flash_write_address;
	uint32_t loc_fde = flash_data_erase;
	uint32_t dt_erase_lst = 0;
	user_debug("flash_write_address %d\r\n",flash_write_address);
	user_debug("flash_data_erase %d\r\n",flash_data_erase);
	if((loc_fwa&ONBOARD_FLASH)<Flash_data_lst_addr)
	{
		if(loc_fde<=(loc_fwa+wr_sz))
		{
			flash_clear_memory(ERASE_4KB,(loc_fde&ONBOARD_FLASH));
			delay_ms(200);
			loc_fde=loc_fde+4096;
			backup_global_variable(&dt_erase_lst,40,sizeof(flash_data_erase),EEPROM_READ);
			if(flash_data_erase >= (dt_erase_lst+4097))
			{
				flash_data_erase = loc_fde = dt_erase_lst;
			}
			else if((flash_data_erase < dt_erase_lst) && (dt_erase_lst > 4096))
			{
				flash_data_erase = loc_fde = dt_erase_lst;
			}
			backup_global_variable(&flash_data_erase,40,sizeof(flash_data_erase),EEPROM_WRITE);
		}
		else if(loc_fde>(flash_write_address+4097))
		{
			backup_global_variable(&dt_erase_lst,40,sizeof(flash_data_erase),0);
			if(dt_erase_lst>(flash_write_address+4097))
			{
				if(flash_write_address%4096)
				{
					flash_data_erase = loc_fde = ((flash_write_address/4096)+1)*4096;
				}
				else
				{
					flash_data_erase = loc_fde = (flash_write_address/4096)*4096;
				}
			}
			backup_global_variable(&flash_data_erase,40,sizeof(flash_data_erase),EEPROM_WRITE);
		}
	}
	else
	{
		loc_fwa=1+ONBOARD_FLASH;
		loc_fde=1+ONBOARD_FLASH;
		flash_clear_memory(ERASE_4KB,(loc_fde&ONBOARD_FLASH));
		delay_ms(200);
		flash_data_erase = loc_fde= loc_fde+4096;
		backup_global_variable(&loc_fde,40,sizeof(loc_fde),EEPROM_WRITE);
	}
	flash_aai_word_program((loc_fwa&ONBOARD_FLASH),wr_ptr,wr_sz);
	loc_fwa=loc_fwa+wr_sz;
	
	if(loc_fwa>(ERROR_FLASH_ADDRESS))
	{
		backup_global_variable(&loc_fwa,10,sizeof(loc_fwa),EEPROM_READ);
		if(loc_fwa>(ERROR_FLASH_ADDRESS))
		{
			backup_global_variable(&loc_fde,40,sizeof(loc_fde),EEPROM_READ);
			if(loc_fde>(ERROR_FLASH_ADDRESS))
			{
				flash_data_erase = loc_fde=0;
				flash_write_address = loc_fwa=0;
				backup_global_variable(&loc_fde,40,sizeof(loc_fde),EEPROM_WRITE);
			}
			else
			{
				loc_fwa = (loc_fde/wr_sz)*wr_sz;
			}
		}
	}
	flash_write_address = loc_fwa;
	flash_data_erase = loc_fde;
	backup_global_variable(&flash_write_address,10,sizeof(flash_write_address),EEPROM_WRITE);
}



void flash_buffer_read(uint8_t *rd_ptr,uint16_t rd_sz)
{
	uint32_t loc_fwa = flash_write_address;
	uint32_t loc_fde = flash_data_erase;
	uint32_t loc_fwra = flash_webclient_read_address;
	user_debug("flash_webclient_read_address %d\r\n",flash_webclient_read_address);
	if(loc_fwra>=Flash_data_lst_addr)
	{
		if(loc_fwa>ONBOARD_FLASH)
		{
			flash_webclient_read_address = loc_fwra = 0;
			flash_write_address = loc_fwa = loc_fwa & ONBOARD_FLASH;
			flash_data_erase =    loc_fde = loc_fde & ONBOARD_FLASH;
			backup_global_variable(&flash_data_erase,40,sizeof(flash_data_erase),1);
			backup_global_variable(&flash_write_address,10,sizeof(flash_write_address),1);
		}
	}
	if((loc_fwa&ONBOARD_FLASH)>=loc_fwra)
	{
		data_packet_size=loc_fwa-loc_fwra;
	}
	else
	{
		if(loc_fwa<=loc_fwra)
		{
			flash_webclient_read_address = loc_fwra = 0;
			flash_write_address = loc_fwa = loc_fwa & ONBOARD_FLASH;
			flash_data_erase =    loc_fde = loc_fde & ONBOARD_FLASH;
			data_packet_size=0;
			backup_global_variable(&flash_data_erase,40,sizeof(flash_data_erase),1);
			backup_global_variable(&flash_write_address,10,sizeof(flash_write_address),1);
		}
		else
		{
			data_packet_size=Flash_data_lst_addr-loc_fwra;
		}
	}
	if(data_packet_size>=rd_sz)
	{
		flash_read(flash_webclient_read_address,rd_ptr,rd_sz);	//	copy the data from flash to tcp buffer
	}
	
}

void flash_rd_add_add(void)
{
	flash_webclient_read_address=flash_webclient_read_address+sizeof(GLOBAL_PKT);
	backup_global_variable(&flash_webclient_read_address,30,sizeof(flash_webclient_read_address),EEPROM_WRITE);
}
void flash_add_update(bool updown)
{
	NVIC_DisableIRQ(EIC_IRQn);
/*	if(updown)
	{
		flash_obj.flash_write_address = htonl32_wp(flash_write_address);
		flash_obj.flash_read_address = htonl32_wp(flash_read_address);
		flash_obj.flash_webclient_read_address = htonl32_wp(flash_webclient_read_address);
		flash_obj.flash_data_erase = htonl32_wp(flash_data_erase);
		flash_obj.Flash_data_lst_addr = htonl32_wp(Flash_data_lst_addr);
		
	}
	else
	{
		flash_write_address = ntohl_32(flash_obj.flash_write_address);
		flash_read_address = ntohl_32(flash_obj.flash_read_address);
		flash_webclient_read_address = ntohl_32(flash_obj.flash_webclient_read_address);
		flash_data_erase = ntohl_32(flash_obj.flash_data_erase);
		Flash_data_lst_addr = ntohl_32(flash_obj.Flash_data_lst_addr);
		backup_global_variable(&flash_write_address,10,sizeof(flash_write_address),1);
		backup_global_variable(&flash_read_address,20,sizeof(flash_read_address),1);
		backup_global_variable(&flash_webclient_read_address,30,sizeof(flash_webclient_read_address),1);
		backup_global_variable(&flash_data_erase,40,sizeof(flash_data_erase),1);
	}
*/	NVIC_EnableIRQ(EIC_IRQn);
}
