/*
 * Flash_Api.h
 *
 * Created: 06/01/2018 12:32:11 PM
 *  Author: Arjun
 */ 


#ifndef FLASH_API_H_
#define FLASH_API_H_
#include <asf.h>


#define ONBOARD_FLASH 0x7ffff  // refer datasheet of flash to find the last address of flash. 4Mbit = 512KByte = 0x7ffff
#define ERROR_FLASH_ADDRESS  (ONBOARD_FLASH + ONBOARD_FLASH + 3)
#define DATA_SIZE	 10

struct __attribute__((__packed__)) device_flash
{
	uint8_t volatile rtclock_time[6];
	uint32_t flash_write_address;  // write address of flash initialized to zero
	uint32_t flash_read_address;   // write address of flash initialized to zero
	uint32_t flash_webclient_read_address;
	uint32_t flash_data_erase;
	uint32_t Flash_data_lst_addr;
};



void flash_init(void);

void flash_address_init(void);

void flash_buffer_write(uint8_t *wr_ptr, uint16_t wr_sz);

void flash_buffer_read(uint8_t *rd_ptr,uint16_t rd_sz);

void flash_add_update(bool updown);

void flash_rd_add_add(void);


#endif /* FLASH_API_H_ */