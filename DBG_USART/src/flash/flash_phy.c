#include "flash_phy.h"

void flash_PORT_init(void)
{
	#ifdef CONF_FLASH
	struct port_config pin_conf;
	port_get_config_defaults(&pin_conf);

	// Set pin direction to output
/*	pin_conf.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(SPI_MOSI, &pin_conf);
	port_pin_set_output_level(SPI_MOSI, false);

	pin_conf.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(SPI_SCK, &pin_conf);
	port_pin_set_output_level(SPI_SCK, false);

	pin_conf.direction = PORT_PIN_DIR_INPUT;
	pin_conf.input_pull = PORT_PIN_PULL_NONE;
	port_pin_set_config(SPI_MISO, &pin_conf);
*/	
	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(DF_CS, &pin_conf);
	port_pin_set_output_level(DF_CS, DF_CS_ACTIVE);
	#endif
}


uint8_t configure_flash_spi(void)
{
	//! [config]
#ifdef CONF_SPI
	flash_PORT_init();
	
	uint8_t ret_status;
	struct spi_config config_spi_master;
	struct spi_slave_inst_config slave_dev_config;

	spi_slave_inst_get_config_defaults(&slave_dev_config);
	slave_dev_config.ss_pin = DF_CS;
	spi_attach_slave(&flash_slave, &slave_dev_config);
	spi_get_config_defaults(&config_spi_master);

	config_spi_master.mux_setting = SPI_SIGNAL_MUX_SETTING_D;
	config_spi_master.pinmux_pad0 = SPI_MOSI;
	config_spi_master.pinmux_pad1 = SPI_SCK;
	config_spi_master.pinmux_pad2 = PINMUX_UNUSED;
	config_spi_master.pinmux_pad3 = SPI_MISO;
	config_spi_master.transfer_mode=SPI_TRANSFER_MODE_3;
	config_spi_master.mode_specific.master.baudrate= 2000000;
	ret_status = spi_init(&flash_spi, SPI_MODULE, &config_spi_master);
	//user_debug("spi_init Status %d\r\n",ret_status);
	spi_enable(&flash_spi);
	
	return ret_status;
#endif
}


/***********************************************************************************************************************************/
/*
*	Function Name: spi_flash_init
*	Description:   Initializes the SPI & configures the CS pin.
*/
/***********************************************************************************************************************************/
void spi_flash_init(void)
{
	flash_memory_write_enable();
	flash_write_status_reg(0x00); // the user can access the whole flash memory of 4MB
	flash_memory_write_disable();
}



/**************************************************************************************************************************************/
/*
*	Function name: flash_jdec_read_id
*	Description:   Reads Manufacturer Id, Memory type & Memory Capacity of Flash memory
*/
/*************************************************************************************************************************************/
void flash_jdec_read_id(void)
{
	uint8_t data[4];
	uint8_t temp=0x9f;
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);  // read id command for flash for getting deviceid
	spi_read_buffer_wait(&flash_spi,data,3,0xff);
	
	for(int i=0;i<3;i++)
	{	
		user_debug(" jdec read %d value %x\r\n",i,data[i]);
	}
	
	delay_us(1);
	spi_select_slave(&flash_spi, &flash_slave, false);

}

/**********************************************************************************************************************************/
/*
*	Function name: flash_memory_write_enable
*	Description:   it allows to perform any changes in the flash memory like
*					writing to memory,status reg,erase,etc.
*/
/**********************************************************************************************************************************/
void flash_memory_write_enable(void)
{
	uint8_t data=0x06;
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&data,1);               // OPCODE FOR ENABLING WRITE TO MEMORY
	delay_us(10);
	spi_select_slave(&flash_spi, &flash_slave, false);
}


/************************************************************************************************************************************/
/*
*	Function name: flash_memory_write_disable
*	Description:   It does not allow to perform any changes in the flash memory like
*					writing to memory,status reg,erase,etc.
*/
/************************************************************************************************************************************/
void flash_memory_write_disable(void)
{
	uint8_t temp=0x04;
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);               // OPCODE FOR DISABLING WRITES TO MEMORY
	delay_us(10);
	spi_select_slave(&flash_spi, &flash_slave, false);
}


/*********************************************************************************************************************************/
/*
*	Function name: flash_read_status_reg
*	Description:   it gets the status of flash memory
*/
/**********************************************************************************************************************************/
uint8_t flash_read_status_reg_1(void)
{
	uint8_t data;
	uint8_t check=0x01;
	uint8_t temp =0x05;
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);               // OPCODE FOR READING STATUS REG
	spi_read_buffer_wait(&flash_spi,&data,1,0xff);
	while(check)
	{
		spi_read_buffer_wait(&flash_spi,&data,1,0xff);
		check=data & 0x01;
	}
	while(check)
	{
		spi_read_buffer_wait(&flash_spi,&data,1,0xff);
		check=data & 0x02;
	}
	delay_us(1);
	spi_select_slave(&flash_spi, &flash_slave, false);
	return data;
}


uint8_t flash_read_status_reg_2(void)
{
	uint8_t data;
	uint8_t temp =0x35;
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);               // OPCODE FOR READING STATUS REG
	spi_read_buffer_wait(&flash_spi,&data,1,0xff);
	spi_read_buffer_wait(&flash_spi,&data,1,0xff);
	delay_us(1);
	spi_select_slave(&flash_spi, &flash_slave, false);
	return data;
}

/**************************************************************************************************************************************/
/*
*	Function name: flash_write_status_reg
*	Description:   writes the data to status reg. it internally calls flash_memory_write_enable(); & flash_status_reg_enable();
*/
/****************************************************************************************************************************************/
void flash_write_status_reg(uint8_t data)
{
	uint8_t tmp=0x01;
	flash_memory_write_enable();
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&tmp,1);               // OPCODE FOR WRITING TO STATUS REG
	spi_write_buffer_wait(&flash_spi,&data,1);
	spi_write_buffer_wait(&flash_spi,&data,1);
	delay_us(1);
	spi_select_slave(&flash_spi, &flash_slave, false);
	
}


/**********************************************************************************************************************************************/
/*
*	Function name	: flash_clear_memory
*	Description		:   It clears the flash memory as per the parameter send.
*	Parameter		:  ERASE_4KB / ERASE_32KB / ERASE_64KB / ERASE_ALL
*					:  address
*/
/**********************************************************************************************************************************************/
void flash_clear_memory(uint8_t size,uint32_t address)
{
	uint8_t temp=size;
	uint8_t add_var[3];
	add_var[0] = (address &0x00ff0000)>>16;
	add_var[1] = (address &0x0000ff00)>>8;
	add_var[2] = (address &0x000000ff);
	switch(size)
	{
		case ERASE_4KB:
				/* erasing only 4KB of memory  */
				flash_memory_write_enable();
				delay_ms(15);
		        spi_select_slave(&flash_spi, &flash_slave, true);
				delay_us(5);
				spi_write_buffer_wait(&flash_spi,&temp,1); 
				spi_write_buffer_wait(&flash_spi,&add_var[0],1);
				spi_write_buffer_wait(&flash_spi,&add_var[1],1);
				spi_write_buffer_wait(&flash_spi,&add_var[2],1);
				delay_us(10);
				spi_select_slave(&flash_spi, &flash_slave, false);
				delay_ms(350);
				flash_memory_write_disable();
				flash_read_status_reg_1();
				break;
		case ERASE_32KB:
				/* erasing only 32KB of memory  */
				flash_memory_write_enable();
				delay_ms(15);
				spi_select_slave(&flash_spi, &flash_slave, true);
				delay_us(1);
		        spi_write_buffer_wait(&flash_spi,&temp,1);
				spi_write_buffer_wait(&flash_spi,&add_var[0],1);
				spi_write_buffer_wait(&flash_spi,&add_var[1],1);
				spi_write_buffer_wait(&flash_spi,&add_var[2],1);
				delay_us(1);
				spi_select_slave(&flash_spi, &flash_slave, false);
				delay_ms(350);
				delay_s(1);
//				flash_memory_write_disable();
				flash_read_status_reg_1();
		        break;
		case ERASE_64KB:
				/* erasing only 64KB of memory  */
				flash_memory_write_enable();
				delay_ms(15);
		    	spi_select_slave(&flash_spi, &flash_slave, true);
				delay_us(1);
			    spi_write_buffer_wait(&flash_spi,&temp,1);
				spi_write_buffer_wait(&flash_spi,&add_var[0],1);
				spi_write_buffer_wait(&flash_spi,&add_var[1],1);
				spi_write_buffer_wait(&flash_spi,&add_var[2],1);
				delay_us(1);
		    	spi_select_slave(&flash_spi, &flash_slave, false);
				delay_ms(350);
				delay_s(2);
//				flash_memory_write_disable();
				flash_read_status_reg_1();
			    break;
		case ERASE_ALL:
				/* erasing whole 4MB of memory  */
				temp=0x60;
				flash_memory_write_enable();
				delay_ms(1);
				spi_select_slave(&flash_spi, &flash_slave, true);
				delay_us(1);
				spi_write_buffer_wait(&flash_spi,&temp,1);
				delay_us(10);
				spi_select_slave(&flash_spi, &flash_slave, false);
				delay_ms(10);
//				flash_memory_write_disable();
				flash_read_status_reg_1();
				break;
		default:
				break;
	}
}

/**********************************************************************************************************************************************/
/*
*	Function name	:	flash_byte_program
*	Description		:	it is used to write single byte of data to flash memory
*	Parameter		:	address & data.
*/
/*********************************************************************************************************************************************/
void flash_byte_program(uint32_t address,uint8_t value)
{
	uint8_t temp=0x02;
	uint8_t add_var[3];
	add_var[0] = (address &0x00ff0000)>>16;
	add_var[1] = (address &0x0000ff00)>>8;
	add_var[2] = (address &0x000000ff);
	flash_memory_write_enable();
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);                    // OPCODE FOR WRITING SINGLE BYTE
	spi_write_buffer_wait(&flash_spi,&add_var[0],1);
	spi_write_buffer_wait(&flash_spi,&add_var[1],1);
	spi_write_buffer_wait(&flash_spi,&add_var[2],1);
	spi_write_buffer_wait(&flash_spi,&value,1);
	delay_us(1);
	spi_select_slave(&flash_spi, &flash_slave, false);
	flash_memory_write_disable();
}


/************************************************************************/
/*
*	Function name: flash_aai_word_program
*	Description:   it is used to write number of byte of data to flash memory
*	Parameter:     address, data & length (no of bytes).
*	Note: length should be always even numbers.
*/
/************************************************************************/
void flash_aai_word_program(uint32_t address,uint8_t *value,uint8_t length)
{
	uint8_t temp=0x02;
	uint8_t loc_len=0;
	uint8_t add_var[3];
	loc_len = (address+length)%256;
	if(loc_len>=length)
	{
		loc_len=0;
	}
	length=length-loc_len;
	add_var[0] = (address &0x00ff0000)>>16;
	add_var[1] = (address &0x0000ff00)>>8;
	add_var[2] = (address &0x000000ff);
	flash_memory_write_enable();
	delay_ms(15);
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);                     // OPCODE FOR ENABLING AAI MODE WRITING
	spi_write_buffer_wait(&flash_spi,add_var,3);
	spi_write_buffer_wait(&flash_spi,value,length);
	delay_us(10);
	spi_select_slave(&flash_spi, &flash_slave, false);
	delay_ms(5);
//	flash_memory_write_disable();
	delay_ms(1);
	flash_read_status_reg_1();
	//user_debug("\r\nflash write start\r\n");
	if(loc_len>0)
	{
		address=address+length;
		add_var[0] = (address &0x00ff0000)>>16;
		add_var[1] = (address &0x0000ff00)>>8;
		add_var[2] = (address &0x000000ff);
		flash_memory_write_enable();
		delay_ms(15);
		spi_select_slave(&flash_spi, &flash_slave, true);
		delay_us(1);
		spi_write_buffer_wait(&flash_spi,&temp,1);
		spi_write_buffer_wait(&flash_spi,add_var,3);
		spi_write_buffer_wait(&flash_spi,&value[length],loc_len);
		delay_us(10);
		spi_select_slave(&flash_spi, &flash_slave, false);
		delay_ms(5);
//		flash_memory_write_disable();
		flash_read_status_reg_1();
	}
		flash_memory_write_disable();
}


/**********************************************************************************************************************************************/
/*
*	Function name: flash_read
*	Description:   it gets the data from flash memory. it runs at max 25mhz
*	Parameter:     address,length (no of bytes),array or pointer for storing a read data.
*/
/**********************************************************************************************************************************************/
void flash_read(uint32_t address,uint8_t *value,uint16_t length)
{
	uint8_t temp=0x03;
	uint8_t add_var[3];
	add_var[0] = (address &0x00ff0000)>>16;
	add_var[1] = (address &0x0000ff00)>>8;
	add_var[2] = (address &0x000000ff);
	NVIC_DisableIRQ(EIC_IRQn);
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);                     // OPCODE FOR READ MEMORY
	spi_write_buffer_wait(&flash_spi,add_var,3);
	spi_read_buffer_wait(&flash_spi,value,length,0xff);
	delay_ms(1);
	spi_select_slave(&flash_spi, &flash_slave, false);
	NVIC_EnableIRQ(EIC_IRQn);
}

/*********************************************************************************************************************************************/
/*
*	Function name: flash_high_speed_read
*	Description:   it gets the data from flash memory. it runs at max 80mhz
*	Parameter:     address,length (no of bytes),array or pointer for storing a read data.
*/
/*********************************************************************************************************************************************/
void flash_high_speed_read(uint32_t address,uint8_t *value,uint16_t length)
{
	uint8_t temp=0x0b;
	uint8_t add_var[3];
	add_var[0] = (address &0x00ff0000)>>16;
	add_var[1] = (address &0x0000ff00)>>8;
	add_var[2] = (address &0x000000ff);
	spi_select_slave(&flash_spi, &flash_slave, true);
	delay_us(1);
	spi_write_buffer_wait(&flash_spi,&temp,1);                   // OPCODE FOR HIGH SPEED READ
	spi_write_buffer_wait(&flash_spi,add_var,3);
	spi_read_buffer_wait(&flash_spi,value,length,0xff);
	delay_us(1);
	spi_select_slave(&flash_spi, &flash_slave, false);
}

