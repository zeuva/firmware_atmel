/*
 * GSM_M66.c
 *
 * Created: 14-05-2015 13:13:31
 *  Author: User
 */ 
#include <main.h>
#include "GSM_M66.h"
#include <string.h>
#include <ctype.h>
#include <timer/timeout.h>
#include <conversion_library/conv_lib.h>
#include <usercalendar/user_calendar.h>

struct calendar_date date_tmp;

extern uint64_t ds1343_tick;


	uint8_t rec_data;
	void configure_GSM_uart()
	{
#ifdef CONF_GSM
		uint8_t gsm_instance_index;
		struct usart_config usart_conf;
		usart_get_config_defaults(&usart_conf);
		usart_conf.mux_setting = USART_RX_1_TX_0_XCK_1;
		usart_conf.pinmux_pad0 = GSM_RX;
		usart_conf.pinmux_pad1 = GSM_TX;
		usart_conf.pinmux_pad2 = PINMUX_UNUSED;
		usart_conf.pinmux_pad3 = PINMUX_UNUSED;
		usart_conf.baudrate    = 115200;
		usart_conf.character_size = USART_CHARACTER_SIZE_8BIT;
		usart_conf.parity = USART_PARITY_NONE;
		usart_conf.stopbits = USART_STOPBITS_1;

		while (usart_init(&gsm_uart_module, GSM_MODULE, &usart_conf) != STATUS_OK) {
		}
	
		// Inject our own interrupt handler

		gsm_instance_index = _sercom_get_sercom_inst_index(GSM_MODULE);
		_sercom_set_handler(gsm_instance_index, gsm_rx_handler);

		// Enable the UART transceiver
		usart_enable(&gsm_uart_module);
		usart_enable_transceiver(&gsm_uart_module, USART_TRANSCEIVER_TX);
		usart_enable_transceiver(&gsm_uart_module, USART_TRANSCEIVER_RX);
		//	 ..and the RX Complete interrupt
		memset(searchForgsm,0,20);
		memcpy(searchForgsm,"\r\nOK\r\n",6);
		memset(searchForgsm_1,0,20);
		memcpy(searchForgsm_1,"\r\nALREADYCONNECT\r\n",18);
		memset(searchForgsm_2,0,10);
		memcpy(searchForgsm_2,"ERROR",5);
		memset(searchForgsm_3,0,13);
		memcpy(searchForgsm_3,"+PDP DEACT\r\n",12);
		memset(searchForgsm_4,0,20);
		memcpy(searchForgsm_4,"\r\nCONNECT FAIL\r\n",16);

#endif
	}

	void GSM_rx_Buffer_reset( void )
	{
#ifdef CONF_GSM

		rx_i2 = rx_i = rx_wr_i = 0;           //Init variables
		rx_i4=0;
		rx_ack = 0;     //Zero overflow flag
		memset(rx_buffer,0,300);
#endif
	}

	void GSM_rx_on( void )
	{
#ifdef CONF_GSM

		((SercomUsart *)GSM_MODULE)->INTENSET.reg = SERCOM_USART_INTFLAG_RXC;
#endif
	}

	/*! \brief RX interrupt disable
	 *
	 *  \param    void
	 *
	 *  \retval   void
	 */
	void GSM_rx_off( void )
	{
#ifdef CONF_GSM

		((SercomUsart *)GSM_MODULE)->INTENCLR.reg = SERCOM_USART_INTFLAG_RXC;
#endif
	}

	void gsm_rx_handler(uint8_t instance)
	{
#ifdef CONF_GSM

		SercomUsart *const usart_hw = (SercomUsart *)GSM_MODULE;
		uint16_t volatile interrupt_status;
		uint8_t error_code;

		// Wait for synch to complete
/*		#if defined(FEATURE_SERCOM_SYNCBUSY_SCHEME_VERSION_1)
		while (usart_hw->STATUS.reg & SERCOM_USART_STATUS_SYNCBUSY) {
		}
		#elif defined(FEATURE_SERCOM_SYNCBUSY_SCHEME_VERSION_2)
		while (usart_hw->SYNCBUSY.reg) {
		}
		#endif*/

		// Read and mask interrupt flag register
		interrupt_status = usart_hw->INTFLAG.reg;
	
		if (interrupt_status & SERCOM_USART_INTFLAG_RXC) {
			// Check for errors
			error_code = (uint8_t)(usart_hw->STATUS.reg & SERCOM_USART_STATUS_MASK);
			if (error_code) {
				// Only frame error and buffer overflow should be possible
				if (error_code &
				(SERCOM_USART_STATUS_FERR | SERCOM_USART_STATUS_BUFOVF)) {
					usart_hw->STATUS.reg =
					SERCOM_USART_STATUS_FERR | SERCOM_USART_STATUS_BUFOVF;
					} else {
					// Error: unknown failure
				}
				// All is fine, so push the received character into our queue
				} else {
					rec_data = (usart_hw->DATA.reg & SERCOM_USART_DATA_MASK);
					if((searchForgsm[rx_i] == rec_data) || (searchForgsm_1[rx_i] == rec_data))                   //Test response match
					{
						rx_i++;
						if((!searchForgsm[rx_i])||(!searchForgsm_1[rx_i]))                      //End of new_message string...received new message!
						{
							rx_i = 0;
							rx_ack = 1;
						}
					}
					if((searchForgsm_2[rx_i2] == rec_data))                   //Test response match
					{
						rx_i2++;
						if((!searchForgsm_2[rx_i2]))                      //End of new_message string...received new message!
						{
							rx_i2 = 0;
							rx_ack = 2;
						}
					}
					if((searchForgsm_4[rx_i4] == rec_data))                   //Test response match
					{
						rx_i4++;
						if((!searchForgsm_4[rx_i4]))                      //End of new_message string...received new message!
						{
							rx_i4 = 0;
							rx_ack = 2;
						}
					}
					if((searchForgsm_3[rx_i3] == rec_data))                   //Test response match
					{
						rx_i3++;
						if((!searchForgsm_3[rx_i3]))                      //End of new_message string...received new message!
						{
							rx_i3 = 0;
							gsm_reinit_status = 1;
							gsm_init_done = 1;
						}
					}
					else
					{
						rx_i3 = 0;
					}

					rx_buffer[ rx_wr_i++ ] = rec_data;                  //Store new data
				}
			} else {
			// Error: only RX interrupt should be enabled
		}
#endif
	}

	void GSM_M66_configure_baudrate(void)

	{
#ifdef CONF_GSM

		gsm_user_write("AT\r\n",4);
		//user_debug("Baudrate set : %s\r\n",rx_buffer);
		//GSM_rx_Buffer_reset();
#endif
	}

	int GSM_check_acknowledge( void )
	{
#ifdef CONF_GSM

		if(rx_ack==1)                      //End of new_message string...received new message!
		{
			rx_ack = 0;
			return 1;
		}
		if(rx_ack==2)                      //End of new_message string...received new message!
		{
			return 1;
		}
		return 0;
#endif
	}

	void gsm_user_write(uint8_t *data,uint16_t usr_len)
	{
#ifdef CONF_GSM

		uint16_t length=usr_len;
		SercomUsart *const usart_hw = (SercomUsart *)GSM_MODULE;
		while(length--)
		{
			uint8_t data_to_send = *data;
			while(!((usart_hw->INTFLAG.reg)&SERCOM_USART_INTFLAG_DRE));
			usart_hw->DATA.reg = (data_to_send & SERCOM_USART_DATA_MASK);		
			data++;
		}
	
#endif
	}

	void gsm_m66_off(void)
	{
#ifdef CONF_GSM

		ioport_set_pin_level(GSM_PWRKEY,false);
		delay_ms(800);
		ioport_set_pin_level(GSM_PWRKEY,true);
		delay_s(4);
		delay_s(4);
		delay_s(4);
		delay_s(4);
#endif
	}

	void gsm_m66_on(void)
	{
#ifdef CONF_GSM
		delay_ms(100);
		ioport_set_pin_level(GSM_PWRKEY,false);
		delay_s(1);
		delay_ms(400);
#endif
	}

	uint8_t GSM_M66_init(uint8_t *mydebug)
	{
#ifdef CONF_GSM
		uint8_t *ret,*ret1;
		uint8_t copy_len;
		int gsm_retry=0;
		uint8_t gsm_sp[30]= {0};
		GSM_rx_on();
		user_debug("Configuring Baud Rate\r\n");
		GSM_M66_configure_baudrate();
		delay_ms(100);
		GSM_M66_configure_baudrate();
		delay_ms(100);
		cpu_irq_enable();
		for(int i=0;i<5;i++)
		{
			gsm_timeout =3;
			configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
			GSM_M66_configure_baudrate();
			while ((!GSM_check_acknowledge()) && (gsm_timeout>0))
			{
				delay_s(1);
			}
			GSM_rx_Buffer_reset();
			delay_ms(1);
		}
		user_debug("Done configuring Baud Rate\r\n");
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if(gsm_timeout==0)
		{
			user_debug("Baud Rate Not Set\r\n");
			sprintf(mydebug,"Baud Rate Not Set\r\n");
			return 1;
		}		


		gsm_timeout =1;
		user_debug(AT_ERROR_SET);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_ERROR_SET,sizeof(AT_ERROR_SET));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("%s",rx_buffer);
			user_debug("AT_ERROR NOT SET\r\n");
			sprintf(mydebug,"ERRORNOTSET %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
	
		user_debug("ERROR: %s",rx_buffer);
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		user_debug(AT_ECHO_OFF);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_ECHO_OFF,sizeof(AT_ECHO_OFF));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("ECHO NOT OFF\r\n");
			sprintf(mydebug,"ECHONOTSET %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("ECHO_OFF: %s",rx_buffer);
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		user_debug(AT_IMEI_NO);
		gsm_user_write(AT_IMEI_NO,sizeof(AT_IMEI_NO));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not IMEI_NO\r\n");
			user_debug("IMEI NO: %s",rx_buffer);
			sprintf(mydebug,"IMEI NOT %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("IMEI NO: %s",rx_buffer);
		ret = (uint8_t *)strstr(&rx_buffer[2],"\r");
		if(ret)
		{
			copy_len=ret-rx_buffer-2;
			memcpy(gsm_tmp.gsm_imei,&rx_buffer[2],copy_len);			
		}

		//	user_debug("IMEI NO: %s",gsm_obj.gsm_imei);
		GSM_rx_Buffer_reset();

		gsm_retry=10;
		CHECK_SIMPIN:

		if(gsm_retry<=0)
		{
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}

		gsm_timeout =3;
		user_debug(AT_SIM_PIN_QUERY);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_SIM_PIN_QUERY,sizeof(AT_SIM_PIN_QUERY));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			gsm_retry -=1;
			delay_s(1);
			sprintf(mydebug,"SIM %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			user_debug("Not SIM_PIN_QUERY %s\r\n",rx_buffer);
			goto CHECK_SIMPIN;
		}
		user_debug("SIM_PIN: %s",rx_buffer);
		if(!strstr(rx_buffer,"+CPIN: READY"))
		{
			gsm_retry -=1;
			sprintf(mydebug,"SIMNOTSET %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(1);
			goto CHECK_SIMPIN;
		}
		GSM_rx_Buffer_reset();
		//	user_debug("AT_SIM_PIN_QUERY\r\n");
	
		gsm_retry=10;
		CHECK_INIT:
		if(gsm_retry<=0)
		{
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}

		gsm_timeout =1;
		user_debug("AT+QINISTAT\r\n");
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write("AT+QINISTAT\r\n",strlen("AT+QINISTAT\r\n"));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("AT_ERROR NOT SET\r\n");
			sprintf(mydebug,"QINISTAT %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(1);
			gsm_retry -=1;
			goto CHECK_INIT;
		}
	

		user_debug("QINISTAT: %s",rx_buffer);
		if(strstr(rx_buffer,"+QINISTAT: 3")){}
		else
		{
			gsm_retry-=1;
			sprintf(mydebug,"%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(1);
			goto CHECK_INIT;
		}
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		user_debug(AT_SIM_CCID);
		gsm_user_write(AT_SIM_CCID,sizeof(AT_SIM_CCID));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not SIM_CCID\r\n");
			sprintf(mydebug,"CCID %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		//	user_debug("AT_SIM_CCID\r\n");
		user_debug("CCID: %s",rx_buffer);
		ret = (uint8_t *)strstr(&rx_buffer[2],"\r");
		if(ret)
		{
			copy_len=ret-rx_buffer-2;
			memcpy(gsm_tmp.sim_ccid,&rx_buffer[2],copy_len);			
		}
		//	user_debug("CCID: %s",gsm_obj.sim_ccid);
		GSM_rx_Buffer_reset();

		gsm_retry=60;
		CHECK_CSQ:
		if(gsm_retry<=0)
		{
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}

		gsm_timeout =1;
		user_debug(AT_SIGNAL_QUALITY);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_SIGNAL_QUALITY,sizeof(AT_SIGNAL_QUALITY));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not SIGNAL_QUALITY\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"CSQ %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(1);
			gsm_retry -=1;
			goto CHECK_CSQ;
		}
		user_debug("%s",rx_buffer);
		if(strstr(rx_buffer,"+CSQ: 99"))
		{
			sprintf(mydebug,"%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(1);
			gsm_retry -=1;
			goto CHECK_CSQ;
		}
		GSM_rx_Buffer_reset();


/////////////////////new AT command added 12jan

if (0)
{

//step1

		gsm_retry=20;
		CHECK_SMS_step1:
		if(gsm_retry<=0)
		{
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}
		
		gsm_timeout =1;
		user_debug(AT_GET_READY_FOR_SMS);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_GET_READY_FOR_SMS,sizeof(AT_GET_READY_FOR_SMS));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not able to set sms step1\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"sms step1 %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(1);
			gsm_retry -=1;
			goto CHECK_SMS_step1;
		}
		user_debug("%s",rx_buffer);
		//if(strstr(rx_buffer,"+CSQ: 99"))
		//{
			//sprintf(mydebug,"%s\r\n",rx_buffer);
			//GSM_rx_Buffer_reset();
			//delay_s(1);
			//gsm_retry -=1;
			//goto CHECK_SMS_step1;
		//}
		GSM_rx_Buffer_reset();
		delay_s(5);

//step1

//step2

gsm_retry=20;
CHECK_SMS_step2:
if(gsm_retry<=0)
{
	GSM_rx_Buffer_reset();
	goto GSM_POWER_DOWN;
}

gsm_timeout =1;
user_debug(AT_SEND_SMS);
configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
gsm_user_write(AT_SEND_SMS,sizeof(AT_SEND_SMS));
while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
if((gsm_timeout==0) || (rx_ack==2))
{
	user_debug("Not able to set sms step2\r\n");
	user_debug("%s\r\n",rx_buffer);
	sprintf(mydebug,"sms step2 %s\r\n",rx_buffer);
	delay_s(3);
	GSM_rx_Buffer_reset();
	delay_s(5);
	gsm_retry -=1;
	//goto CHECK_SMS_step2;
}
user_debug("%s",rx_buffer);
//if(strstr(rx_buffer,"+CSQ: 99"))
//{
//sprintf(mydebug,"%s\r\n",rx_buffer);
//GSM_rx_Buffer_reset();
//delay_s(1);
//gsm_retry -=1;
//goto CHECK_SMS_step1;
//}
//GSM_rx_Buffer_reset();
//delay_s(5);

//step2
//step3

gsm_retry=20;
CHECK_SMS_step3:
if(gsm_retry<=0)
{
	GSM_rx_Buffer_reset();
	goto GSM_POWER_DOWN;
}

gsm_timeout =1;
user_debug(AT_MSG_CONTENT);
configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
gsm_user_write(AT_MSG_CONTENT,sizeof(AT_MSG_CONTENT));
while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
gsm_user_write(AT_MSG_END,sizeof(AT_MSG_END));
while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
if((gsm_timeout==0) || (rx_ack==2))
{
	user_debug("Not able to set sms step3\r\n");
	user_debug("%s\r\n",rx_buffer);
	sprintf(mydebug,"sms step3 %s\r\n",rx_buffer);
	delay_s(1);
	GSM_rx_Buffer_reset();
	delay_s(1);
	gsm_retry=-1;
	goto CHECK_SMS_step3;
}
user_debug("%s",rx_buffer);
//if(strstr(rx_buffer,"+CSQ: 99"))
//{
//sprintf(mydebug,"%s\r\n",rx_buffer);
//GSM_rx_Buffer_reset();
//delay_s(1);
//gsm_retry -=1;
//goto CHECK_SMS_step1;
//}
GSM_rx_Buffer_reset();
delay_s(5);
gsm_retry=20;
goto CHECK_SMS_step1;

//step3

}

		
/////////////////////new AT command added 12jan

		gsm_retry=20;
		CHECK_NETWORK:
		if(gsm_retry<=0)
		{
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}

		gsm_timeout =1;
		user_debug(AT_NETWORK_REG);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_NETWORK_REG,sizeof(AT_NETWORK_REG));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not NETWORK_REG\r\n");
			user_debug("%s\r\n",rx_buffer);
			delay_s(3);
			sprintf(mydebug,"CREG %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			gsm_retry -=1;
			goto CHECK_NETWORK;
		}
		user_debug("AT_NETWORK_REG\r\n");
		user_debug("%s\r\n",rx_buffer);
		if(strstr(rx_buffer,"+CREG: 0,1")){}
		else if (strstr(rx_buffer,"+CREG: 0,5")){}
		else
		{
			sprintf(mydebug,"%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			delay_s(4);
			gsm_retry -=1;
			goto CHECK_NETWORK;
		}
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		user_debug(AT_SERVICE_PROVIDER_NAME);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_SERVICE_PROVIDER_NAME,sizeof(AT_SERVICE_PROVIDER_NAME));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not SERVICE_PROVIDER_NAME\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"APN %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("AT_SERVICE_PROVIDER_NAME\r\n");
		user_debug("NETWORK %s",rx_buffer);
		int j=0;
		ret = (uint8_t *)strstr(rx_buffer,"+QSPN: ");
		if(ret)

		{
			ret += 7;
			ret1 = (uint8_t *)strstr(ret,"\r");
			if(ret1)
			{
				copy_len=ret1-ret;
				while (copy_len>j)
				{
					gsm_sp[j] = toupper(ret[j]);
					j++;
				}
				//			memcpy(gsm_sp,ret,copy_len);
			}
		}
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		user_debug(AT_GPRS_ATTACH);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_GPRS_ATTACH,sizeof(AT_GPRS_ATTACH));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not GPRS_ATTACH\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"CGATT %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("GPRS %s\r\n",rx_buffer);
		user_debug("AT_GPRS_ATTACH\r\n");
		GSM_rx_Buffer_reset();

		gsm_retry=10;
		CHECK_GPRSNETWORK:

		if(gsm_retry<=0)
		{
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}

		gsm_timeout =1;
		user_debug(AT_NETWORK_GPRS_REG);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_NETWORK_GPRS_REG,sizeof(AT_NETWORK_GPRS_REG));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not AT_NETWORK_GPRS_REG\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"CGREG %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			gsm_retry-=1;
			delay_s(1);
			goto CHECK_GPRSNETWORK;
		}
		user_debug("AT_NETWORK_GPRS_REG\r\n");
		user_debug("GPRS_REG %s\r\n",rx_buffer);
		if(strstr(rx_buffer,"+CGREG: 0,1")){}
		else if (strstr(rx_buffer,"+CGREG: 0,5")){}
		else
		{
			sprintf(mydebug,"%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			gsm_retry-=1;
			delay_s(1);
			goto CHECK_GPRSNETWORK;
		}
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		memset(ethernet_buffer,0,BUFFER_SIZE);
		copy_len = sprintf(ethernet_buffer,"AT+QICSGP=1,");
		if((strlen(gsm_obj.gsm_apn)) && (strncmp(gsm_obj.sim_ccid,gsm_tmp.sim_ccid,20)==0))
		{
			copy_len += sprintf(&ethernet_buffer[copy_len],"\"%s\"",gsm_obj.gsm_apn);
		}
		else
		{
			copy_len += get_default_apn(&ethernet_buffer[copy_len],gsm_sp);
		}
		if(strlen(gsm_obj.gsm_username))
		{
			copy_len += sprintf(&ethernet_buffer[copy_len],",\"%s\"",gsm_obj.gsm_username);
		}
		if(strlen(gsm_obj.gsm_pass))
		{
			copy_len += sprintf(&ethernet_buffer[copy_len],",\"%s\"",gsm_obj.gsm_pass);
		}
		copy_len += sprintf(&ethernet_buffer[copy_len],"\r\n");
		user_debug("%s",ethernet_buffer);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(ethernet_buffer,copy_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("NOT REGISTER_APN\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("APN %s\r\n",rx_buffer);
		user_debug("AT_REGISTER_APN\r\n");
		GSM_rx_Buffer_reset();


		gsm_timeout =1;
		user_debug("AT+QISTAT\r\n");
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write("AT+QISTAT\r\n",strlen("AT+QISTAT\r\n"));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("QISTAT NOT\r\n");
			user_debug("%s\r\n",rx_buffer);
			return 1;
		}
		user_debug("QISTAT1 %s\r\n",rx_buffer);
		if(strstr(rx_buffer,"IP INITIAL"))
		{
			goto STEP1;
		}
		else
		{
			if(switch_intial())
			{
				goto GSM_POWER_DOWN;
			}
		}
		GSM_rx_Buffer_reset();

		STEP1:

		gsm_timeout =1;
		user_debug(AT_GET_REGISTER_TCPSTACK);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_GET_REGISTER_TCPSTACK,sizeof(AT_GET_REGISTER_TCPSTACK));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("NOT GET_REGISTER_TCPSTACK\r\n");
			sprintf(mydebug,"REGAPP %s\r\n",rx_buffer);
			user_debug("%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("GET TCP %s\r\n",rx_buffer);
		user_debug("AT_GET_REGISTER_TCPSTACK\r\n");
		GSM_rx_Buffer_reset();

		STEP2:

		gsm_timeout =20;
		user_debug(AT_ACTIVATE_GPRS);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_ACTIVATE_GPRS,sizeof(AT_ACTIVATE_GPRS));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not ACTIVATE_GPRS\r\n");
			user_debug("%s\r\n",rx_buffer);
			sprintf(mydebug,"ACT %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			goto GSM_POWER_DOWN;
		}
		user_debug("ACT_GPRS %s\r\n",rx_buffer);
		user_debug("AT_ACTIVATE_GPRS\r\n");
		GSM_rx_Buffer_reset();

		gsm_init_done=0;
		return 0;

		GSM_POWER_DOWN:
		gsm_timeout =1;
		user_debug("AT+QPOWD=1\r\n");
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write("AT+QPOWD=1\r\n",strlen("AT+QPOWD=1\r\n"));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		delay_s(1);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			ioport_set_pin_level(GSM_PWRKEY,true);
			user_debug("Not AT+QPOWD\r\n");
			user_debug("%s\r\n",rx_buffer);
			return 2;
		}
		user_debug("%s\r\n",rx_buffer);
		user_debug("QPOWD\r\n");
		GSM_rx_Buffer_reset();
		delay_s(5);
		return 1;
#endif
	}

	uint8_t switch_intial(void)
	{
#ifdef CONF_GSM

		gsm_timeout =1;
		user_debug("AT+QIDEACT\r\n");
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write("AT+QIDEACT\r\n",strlen("AT+QIDEACT\r\n"));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		delay_s(1);
		if(gsm_timeout==0)
		{
			user_debug("Not DEACT\r\n");
			user_debug("%s\r\n",rx_buffer);
			return 1;
		}
		user_debug("DEACT %s\r\n",rx_buffer);
		user_debug("IP INITIAl\r\n");
		GSM_rx_Buffer_reset();
		return 0;
#endif
	}

	uint8_t GSM_Send_data_to_Server(uint8_t *buf_snd,uint16_t send_len)
	{
#ifdef CONF_GSM

		uint8_t cmd_buf[20];
		GSM_rx_Buffer_reset();
		memset(searchForgsm,0,20);
		memcpy(searchForgsm,">",1);
		memset(cmd_buf,0,20);

		gsm_timeout =5;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);

		sprintf(cmd_buf,"AT+QISEND=%d\r\n",send_len);
		gsm_user_write(cmd_buf,strlen(cmd_buf));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));

		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		memcpy(searchForgsm,"\r\nOK\r\n",6);

		if((gsm_timeout==0) || (rx_ack==2))
		{
			GSM_rx_Buffer_reset();
			user_debug("SEND Prompt Not came\r\n");
			return 1;
		}
	
		gsm_timeout =5;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(buf_snd,send_len);

		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));

		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);

		if((gsm_timeout==0) || (rx_ack==2))
		{
			GSM_rx_Buffer_reset();
			user_debug("AT_SEND_DATA Timer overflow\r\n");
			return 1;
		}

		user_debug("Data send\r\n");
		GSM_rx_Buffer_reset();
		return 0;

#endif
	}

	uint8_t GSM_Socket_Open(char *proto,uint8_t *ip,uint16_t port,uint8_t dns_state)
 	{

#ifdef CONF_GSM
		uint16_t sck_len=0;
		memset(searchForgsm_1,0,20);
		memcpy(searchForgsm_1,"\r\nALREADYCONNECT\r\n",18);
		GSM_rx_Buffer_reset();
		memset(searchForgsm,0,20);
		memcpy(searchForgsm,"\r\nCONNECTOK\r\n",13);
		user_debug("GSM SEND DATA Task\r\n");
		gsm_timeout =10;
		memset(ethernet_buffer,0,BUFFER_SIZE);
		sck_len=sprintf(ethernet_buffer,"AT+QIOPEN=\"%s\",",proto);
		if(dns_state)
		{
			sck_len=sck_len + sprintf(&ethernet_buffer[sck_len],"\"%s\",",ip);
		}
		else
		{
			sck_len=sck_len + sprintf(&ethernet_buffer[sck_len],"\"%d.%d.%d.%d\",",ip[0],ip[1],ip[2],ip[3]);
		}
		sck_len=sck_len + sprintf(&ethernet_buffer[sck_len],"%d\r\n",port);
		user_debug("%s",ethernet_buffer);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(ethernet_buffer,sck_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		memset(searchForgsm_1,0,20);
		memcpy(searchForgsm_1,"\r\nALREADYCONNECT\r\n",18);
		memset(searchForgsm,0,20);
		memcpy(searchForgsm,"\r\nOK\r\n",6);

		
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("AT_SOCKET_OPEN Timer overflow\r\n");
			user_debug("%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			if (rx_ack==2)
			{
				GSM_Socket_Close();
			}
			return 1;

		}
		GSM_rx_Buffer_reset();
		user_debug("Socket Connect\r\n");
		delay_ms(10);
		return 0;
#endif
	}

	uint8_t GSM_Socket_Close(void)
	{
#ifdef CONF_GSM

		gsm_timeout =1;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_SOCKET_CLOSE,sizeof(AT_SOCKET_CLOSE));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			GSM_rx_Buffer_reset();
			user_debug("AT_SOCKET_CLOSE Timer overflow\r\n");
			return 1;
		}
		GSM_rx_Buffer_reset();
		user_debug("Socket Closed\r\n");
		delay_s(2);
		return 0;
#endif
	}

	uint8_t get_ntp_time(void)
	{
#ifdef CONF_GSM

		uint8_t num=1;
		uint8_t ntp_len;
		//	uint8_t ntp_ip[2][4] = {{125,62,193,121},{120,88,46,10}};
		uint8_t ntp_ip[4][20] = {"1.asia.pool.ntp.org","3.in.pool.ntp.org","2.in.pool.ntp.org","1.in.pool.ntp.org"};
		int8_t ntp_retry_count=4;

		gsm_dns_set(1);
		delay_s(2);
		ntp_retry:
		GSM_rx_Buffer_reset();
		ntp_len=0;
		if(!GSM_Socket_Open("UDP",ntp_ip[ntp_retry_count-1],123,1))
		{
			memset(ethernet_buffer,0,BUFFER_SIZE);
			ntp_len = ntp_request(ethernet_buffer);
			GSM_Send_data_to_Server(ethernet_buffer,ntp_len);
			delay_s(5);
			if(rx_wr_i<40)
			{
				GSM_Socket_Close();
				ntp_retry_count=ntp_retry_count-1;
				if(ntp_retry_count>0)
				{
					goto ntp_retry;
				}
				return 1;
			}
			user_debug("Rx buffer fill %d\r\n",rx_wr_i);
			uint8_t ntp_dp= rx_wr_i-8;
			user_debug("NTP data %x %x",rx_buffer[ntp_dp],rx_buffer[ntp_dp+1]);
			user_debug(" %x %x\r\n",rx_buffer[ntp_dp+2],rx_buffer[ntp_dp+3]);
			//ds1343_tick=((uint32_t)rx_buffer[ntp_dp]<<24)|((uint32_t)rx_buffer[ntp_dp+1]<<16)|((uint32_t)rx_buffer[ntp_dp+2]<<8)|((uint32_t)rx_buffer[ntp_dp+3]);
			//ds1343_tick=ds1343_tick-0x83aa7e80;
			GSM_Socket_Close();
			return 0;
		}
		else
		{
			ntp_retry_count=ntp_retry_count-1;
			if(ntp_retry_count>0)
			{
				delay_s(3);
				goto ntp_retry;
			}
			return 1;
		}
#endif
	}

	uint8_t gsm_dns_set(uint8_t dns_flg)
	{
#ifdef CONF_GSM

		memset(searchForgsm_1,0,20);
		memcpy(searchForgsm_1,"\r\nALREADYCONNECT\r\n",18);
		memset(searchForgsm,0,20);
		memcpy(searchForgsm,"\r\nOK\r\n",6);
		GSM_rx_Buffer_reset();
		gsm_timeout =1;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		memset(ethernet_buffer,0,BUFFER_SIZE);
		sprintf(ethernet_buffer,"AT+QIDNSIP=%d\r\n",dns_flg);
		gsm_user_write(ethernet_buffer,strlen(ethernet_buffer));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not DOMAIN NAME\r\n");
			user_debug("%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
			return 1;
		}
		user_debug("SET DOMAIN NAME\r\n");
		user_debug("%s",rx_buffer);
		GSM_rx_Buffer_reset();
		return 0;
#endif
	}

	uint16_t ntp_request(uint8_t *ntp_buf)
	{
		uint16_t udp_len=0;
	
		while(udp_len<48){
			ntp_buf[udp_len]=0;
			udp_len++;
		}
		ntp_buf[0]=0xE3;
		ntp_buf[1]=0;
		ntp_buf[2]=4;
		ntp_buf[3]=0xFA;
		ntp_buf[4]=0;
		ntp_buf[5]=1;
		ntp_buf[6]=0;
		ntp_buf[7]=0;
		ntp_buf[8]=0;
		ntp_buf[9]=1;
	
		return udp_len;
	}

	uint8_t gsm_call(char *number)
	{
#ifdef CONF_GSM

		uint8_t call_len=0;
		gsm_timeout =23;
		GSM_rx_Buffer_reset();
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		memset(ethernet_buffer,0,BUFFER_SIZE);
		call_len=sprintf(ethernet_buffer,"ATD+91");
		memcpy(&ethernet_buffer[call_len],number,10);
		call_len =call_len+10;
		call_len=call_len + sprintf(&ethernet_buffer[call_len],";\r\n");
		user_debug("Calling Number %s",ethernet_buffer);
		gsm_user_write(ethernet_buffer,call_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		if(gsm_timeout==0)
		{
			configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
			user_debug("Call Not Success\r\n");
			user_debug("%s\r\n",rx_buffer);
			goto forcesuspend;
		}
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		user_debug("%s\r\n",rx_buffer);
		user_debug("Call Successfull");
		return 0;
	
		forcesuspend:
		return 1;
#endif
	}

	uint8_t GSM_Rss(void)
	{
#ifdef CONF_GSM

		uint8_t *ret;
		uint8_t strength=0;
		gsm_timeout =2;
		GSM_rx_Buffer_reset();
		memset(searchForgsm,0,20);
		memcpy(searchForgsm,"\r\nOK\r\n",6);
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(AT_SIGNAL_QUALITY,sizeof(AT_SIGNAL_QUALITY));
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not SIGNAL_QUALITY %d \r\n",rx_ack);
			user_debug("%s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
		}
		if(gsm_timeout==0)
		{
			return 0;
		}
		user_debug("GSMRSS %s\r\n",rx_buffer);
		if(strstr(rx_buffer,"+CSQ:"))
		{
			ret = (uint8_t *)strstr(rx_buffer,"+CSQ:")+5;
			while(*ret!=',')
			{
				if((*ret>0x2f) && (*ret<0x3a))
				{
					strength = (*ret&0x0f)+(strength*10);
				}
				ret++;
			}
			user_debug("AT_SIGNAL_QUALITY %d\r\n",strength);
			return strength;
		}
		else if(strstr(rx_buffer,"AT+CSQ"))
		{
			gsm_timeout =1;
			GSM_rx_Buffer_reset();
			configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
			gsm_user_write(AT_ECHO_OFF,sizeof(AT_ECHO_OFF));
			while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
			configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
			if((gsm_timeout==0) || (rx_ack==2))
			{
				user_debug("ECHO NOT OFF\r\n");
				return 1;
			}
			user_debug("RSSECHO %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
		}
		GSM_rx_Buffer_reset();
		return 2;
#endif
	}

uint8_t get_network_time(void)
{
	uint8_t *ret = NULL;
	uint8_t *ret1 = NULL;
	uint8_t copy_len;

	gsm_timeout =20;
	user_debug("AT+QLTS=1\r\n");
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	gsm_user_write("AT+QLTS=1\r",strlen("AT+QLTS=1\r"));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not QLTS: %s\r\n",rx_buffer);
		return 1;
	}
	//	user_debug_1("AT_SIM_CCID\r\n");
	user_debug("QLTS: %s\r\n",rx_buffer);
	ret = (uint8_t *)strstr(rx_buffer,"+QLTS: ");
	if(ret)
	{
		ret += 8;
		ret1 = (uint8_t *)strstr(ret,"/");
		if(ret1)
		{
			copy_len=ret1-ret;
			date_tmp.year = atodec(ret,copy_len);
			ret = ret1 +1;
			ret1 = (uint8_t *)strstr(ret,"/");
			if(ret1)
			{
				copy_len=ret1-ret;
				date_tmp.month = atodec(ret,copy_len)-1;
				ret = ret1 +1;
				ret1 = (uint8_t *)strstr(ret,",");
				if(ret1)
				{
					copy_len=ret1-ret;
					date_tmp.date = atodec(ret,copy_len)-1;
					ret = ret1 +1;
					ret1 = (uint8_t *)strstr(ret,":");
					if(ret1)
					{
						copy_len=ret1-ret;
						date_tmp.hour = atodec(ret,copy_len);
						ret = ret1 +1;
						ret1 = (uint8_t *)strstr(ret,":");
						if(ret1)
						{
							copy_len=ret1-ret;
							date_tmp.minute = atodec(ret,copy_len);
							ret = ret1 +1;
							ret1 = (uint8_t *)strstr(ret,"+");
							if(ret1)
							{
								copy_len=ret1-ret;
								date_tmp.second = atodec(ret,copy_len);
								ds1343_tick = calendar_date_to_timestamp(&date_tmp);
							}
						}
					}
				}
			}
		}
	}
	GSM_rx_Buffer_reset();
	return 0;
}

	uint8_t get_default_apn(uint8_t *buf,uint8_t *sp)
	{
#ifdef CONF_GSM

		user_debug("Service Provider %s\r\n",sp);
		if(strstr(sp,TATA_DOCOMO))
		{
			strcpy(gsm_tmp.gsm_apn,TATADOCOMO_APN);
			return sprintf(buf,"\"%s\"",TATADOCOMO_APN);
		}
		else if(strstr(sp,RELIANCE))
		{
			strcpy(gsm_tmp.gsm_apn,RELIANCE_APN);
			return sprintf(buf,"\"%s\"",RELIANCE_APN);
		}
		else if(strstr(sp,AIRTEL))
		{
			strcpy(gsm_tmp.gsm_apn,AIRTEL_APN);
			return sprintf(buf,"\"%s\"",AIRTEL_APN);
		}
		else if(strstr(sp,VODAFONE))
		{
			strcpy(gsm_tmp.gsm_apn,VODAFONE_APN);
			return sprintf(buf,"\"%s\"",VODAFONE_APN);
		}
		else if(strstr(sp,AIRCEL))
		{
			strcpy(gsm_tmp.gsm_apn,AIRCEL_APN);
			return sprintf(buf,"\"%s\"",AIRCEL_APN);
		}
		else if(strstr(sp,BSNL))
		{
			strcpy(gsm_tmp.gsm_apn,BSNL_APN);
			return sprintf(buf,"\"%s\"",BSNL_APN);
		}
		else if(strstr(sp,LOOP))
		{
			strcpy(gsm_tmp.gsm_apn,LOOP_APN);
			return sprintf(buf,"\"%s\"",LOOP_APN);
		}
		else if(strstr(sp,UNINOR))
		{
			strcpy(gsm_tmp.gsm_apn,UNINOR_APN);
			return sprintf(buf,"\"%s\"",UNINOR_APN);
		}
		else if(strstr(sp,IDEA))
		{
			strcpy(gsm_tmp.gsm_apn,IDEA_APN);
			return sprintf(buf,"\"%s\"",IDEA_APN);
		}
		else if(strstr(sp,IDEA_NEW))
		{
			strcpy(gsm_tmp.gsm_apn,IDEA_APN);
			return sprintf(buf,"\"%s\"",IDEA_APN);
		}
		else if(strstr(sp,MTNL))
		{
			strcpy(gsm_tmp.gsm_apn,MTNL_APN);
			return sprintf(buf,"\"%s\"",MTNL_APN);
		}
		else 
		{
			return "";
		}
#endif
	}

/*

void put_file_server(void)
{
	uint8_t copy_len;
	int8_t status_retry;
	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug("AT+QIFGCNT=0\r\n");
	gsm_user_write("AT+QIFGCNT=0\r\n",strlen("AT+QIFGCNT=0\r\n"));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug("FTP_CSD: %s",rx_buffer);
	GSM_rx_Buffer_reset();

	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_USER);
	gsm_user_write(FTP_USER,sizeof(FTP_USER));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_USER\r\n");
		user_debug("FTP_USER NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_USER NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();
	
	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_PASS);
	gsm_user_write(FTP_PASS,sizeof(FTP_PASS));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_PASS\r\n");
		user_debug("FTP_PASS NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_PASS NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();

	gsm_timeout =1;
	
	memset(ethernet_buffer,0,BUFFER_SIZE);
	copy_len = sprintf(ethernet_buffer,"AT+QFTPOPEN=");
	copy_len += sprintf(&ethernet_buffer[copy_len],"\"%d.%d.%d.%d\",",dev_comn.server_ip[0],dev_comn.server_ip[1],dev_comn.server_ip[2],dev_comn.server_ip[3]);
	copy_len += sprintf(&ethernet_buffer[copy_len],"21\r\n");
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(ethernet_buffer);
	gsm_user_write(ethernet_buffer,copy_len);
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_OPEN\r\n");
		user_debug("FTP_OPEN NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_OPEN NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();

	status_retry =30;
	FTPST:
	delay_s(3);
	delay_s(3);
	delay_s(3);
	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTPSTATUS);
	gsm_user_write(FTPSTATUS,sizeof(FTPSTATUS));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTPSTATUS\r\n");
		user_debug("FTPSTATUS NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTPSTATUS NO: %s",rx_buffer);
	if(status_retry<=0)
	{
		return 1;
	}
	if(!strstr(rx_buffer,"+QFTPSTAT: OPENED"))
	{
		GSM_rx_Buffer_reset();
		status_retry -= 1;
		goto FTPST;
	}
	GSM_rx_Buffer_reset();

	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);


	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_PATH_UPLOAD);
	gsm_user_write(FTP_PATH_UPLOAD,strlen(FTP_PATH_UPLOAD));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_PATH\r\n");
		user_debug("FTP_PATH NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_PATH NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();



	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);

	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTPSTATUS);
	gsm_user_write(FTPSTATUS,sizeof(FTPSTATUS));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTPSTATUS\r\n");
		user_debug("FTPSTATUS NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTPSTATUS NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();

	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	if(sd_file_open(&fo_ud,&fs,&dir_object,flash_obj.upload_file, FA_READ|FA_OPEN_ALWAYS)==0)
	{
		uint32_t file_sz = f_size(&fo_ud);
		uint32_t file_sz1;
		uint32_t file_count= file_sz/BUFFER_SIZE;
		if(file_sz%BUFFER_SIZE)
		{
			file_count=file_count+1;
		}
		f_close(&fo_ud);

		memset(searchForgsm,0,20);
		memcpy(searchForgsm,"\r\nOK\r\nCONNECT",13);
		memset(ethernet_buffer,0,BUFFER_SIZE);
		copy_len = sprintf(ethernet_buffer,"AT+QFTPPUT=");
		copy_len += sprintf(&ethernet_buffer[copy_len],"\"%s\",",flash_obj.upload_file);
		copy_len += sprintf(&ethernet_buffer[copy_len],"%d,1000\r",file_sz);
		user_debug("%s",ethernet_buffer);

		gsm_timeout =11;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(ethernet_buffer,copy_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		delay_s(3);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not FTPGET\r\n");
			user_debug("FTPGET NO: %s",rx_buffer);
			return 1;
		}
		user_debug("FTPGET NO: %s",rx_buffer);
		GSM_rx_Buffer_reset();
		if(sd_file_open(&fo_ud,&fs,&dir_object,"sd_debug.txt", FA_READ|FA_OPEN_ALWAYS)==0)
		{
			for(uint32_t i=0;i<file_count;i++)
			{
				if((i+1)==file_count)
				{
					file_sz1 = file_sz%BUFFER_SIZE;
				}
				else
				{
					file_sz1 = BUFFER_SIZE;
				}
				if(sd_file_read(&fo_ud,ethernet_buffer,file_sz1)==0)
				{
					gsm_user_write(ethernet_buffer,file_sz1);
					delay_ms(500);
					if((i+1)==file_count)
					{
						user_debug("Flash Write Success\r\n");
						f_close(&fo_ud);
					}
				}
				else
				{
					break;
				}
			}

			memset(searchForgsm,0,20);
			memcpy(searchForgsm,"\r\nOK\r\n",6);
			
			delay_s(3);
			delay_s(3);
			user_debug("FTPSEND NO: %s\r\n",rx_buffer);
			GSM_rx_Buffer_reset();
		}	
	}

	gsm_user_write("+++",strlen("+++"));
	delay_s(1);
	delay_s(1);
	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTPCLOSE);
	gsm_user_write(FTPCLOSE,strlen(FTPCLOSE));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTPCLOSE\r\n");
		user_debug("FTPCLOSE NO: %s\r\n",rx_buffer);
		return 1;
	}
	user_debug("FTPCLOSE NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();
}


void get_ufs_file(void)
{
	uint8_t *ret,*ret1;
	uint8_t copy_len;
	int32_t file_len;
	uint8_t filehandle[20];
	gsm_timeout =2;
	memset(ethernet_buffer,0,BUFFER_SIZE);
	copy_len = sprintf(ethernet_buffer,"%s",FTP_FILE_LIST);
	copy_len += sprintf(&ethernet_buffer[copy_len],"\"%s\"\r",flash_obj.download_file);
	user_debug("%s\n",ethernet_buffer);
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	gsm_user_write(ethernet_buffer,strlen(ethernet_buffer));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_UFS_SIZE\r\n");
		user_debug("%s\r\n",rx_buffer);
		return 1;
	}
	user_debug("FTP_UFS_SIZE %s\r\n",rx_buffer);
	ret = (uint8_t *)strstr(rx_buffer,",")+1;
	ret1 = (uint8_t *)strstr(ret,"\r");
	copy_len = ret1-ret;
	memset(filehandle,0,20);
	memcpy(filehandle,ret,copy_len);
	GSM_rx_Buffer_reset();
	file_len = atodec(filehandle,strlen(filehandle));
	user_debug("file Len %d\r\n",file_len);
	
	gsm_timeout =1;
	GSM_rx_Buffer_reset();
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	memset(ethernet_buffer,0,BUFFER_SIZE);
	copy_len = sprintf(ethernet_buffer,"%s",FTP_UFS_OPEN);
	copy_len += sprintf(&ethernet_buffer[copy_len],"\"%s\",2\r",flash_obj.download_file);
	user_debug("%s\n",ethernet_buffer);
	gsm_user_write(ethernet_buffer,strlen(ethernet_buffer));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_UFS_OPEN\r\n");
		user_debug("%s\r\n",rx_buffer);
		return 1;
	}
	user_debug("FTP_UFS_OPEN: %s",rx_buffer);
	ret = (uint8_t *)strstr(rx_buffer,":")+2;
	ret1 = (uint8_t *)strstr(ret,"\r");
	copy_len = ret1-ret;
	memset(filehandle,0,20);
	memcpy(filehandle,ret,copy_len);
	GSM_rx_Buffer_reset();
	
	while(file_len>0)
	{
		GSM_rx_Buffer_reset();
		memset(ethernet_buffer,0,BUFFER_SIZE);
		copy_len = sprintf(ethernet_buffer,"AT+QFREAD=");
		copy_len += sprintf(&ethernet_buffer[copy_len],"%s,200\r",filehandle);
		user_debug("%s\n",ethernet_buffer);
		gsm_timeout =11;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(ethernet_buffer,copy_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not QFREAD\r\n");
			user_debug("QFREAD NO: %s",rx_buffer);
			return 1;
		}
		user_debug("\r\n");
		for (int i=0;i<rx_wr_i;i++)
		{
			user_debug("%x",rx_buffer[i]);
		}
		user_debug("\r\n");
		file_len = file_len - 200;		
	}

		GSM_rx_Buffer_reset();
		memset(ethernet_buffer,0,BUFFER_SIZE);
		copy_len = sprintf(ethernet_buffer,"%s",FTP_UFS_CLOSE);
		copy_len += sprintf(&ethernet_buffer[copy_len],"%s\r",filehandle);
		user_debug("%s\n",ethernet_buffer);
		gsm_timeout =11;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(ethernet_buffer,copy_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not FTP_UFS_CLOSE\r\n");
			user_debug("FTP_UFS_CLOSE NO: %s",rx_buffer);
			return 1;
		}
		user_debug("FTP_UFS_CLOSE %s\r\n",rx_buffer);


		GSM_rx_Buffer_reset();
		gsm_timeout =11;
		memset(ethernet_buffer,0,BUFFER_SIZE);
		copy_len = sprintf(ethernet_buffer,"%s",FTP_UFS_DELETE);
		copy_len += sprintf(&ethernet_buffer[copy_len],"%s\r",flash_obj.download_file);
		user_debug("%s\n",ethernet_buffer);
		gsm_timeout =11;
		configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
		gsm_user_write(ethernet_buffer,copy_len);
		while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
		configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
		if((gsm_timeout==0) || (rx_ack==2))
		{
			user_debug("Not FTP_UFS_DELETE\r\n");
			user_debug("FTP_UFS_DELETE NO: %s",rx_buffer);
			return 1;
		}
		user_debug("FTP_UFS_DELETE %s\r\n",rx_buffer);


}

void get_file_server(void)
{

	uint8_t copy_len;
	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug("AT+QIFGCNT=0\r\n");
	gsm_user_write("AT+QIFGCNT=0\r\n",strlen("AT+QIFGCNT=0\r\n"));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug("FTP_CSD: %s",rx_buffer);
	GSM_rx_Buffer_reset();


	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_USER);
	gsm_user_write(FTP_USER,strlen(FTP_USER));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_USER\r\n");
		user_debug("FTP_USER NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_USER NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();
	

	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_PASS);
	gsm_user_write(FTP_PASS,strlen(FTP_PASS));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_PASS\r\n");
		user_debug("FTP_PASS NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_PASS NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();


	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_OPEN);
	gsm_user_write(FTP_OPEN,strlen(FTP_OPEN));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_OPEN\r\n");
		user_debug("FTP_OPEN NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_OPEN NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();


	FTPST:
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTPSTATUS);
	gsm_user_write(FTPSTATUS,strlen(FTPSTATUS));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTPSTATUS\r\n");
		user_debug("FTPSTATUS NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTPSTATUS NO: %s",rx_buffer);
	if(!strstr(rx_buffer,"+QFTPSTAT: OPENED"))
	{
		GSM_rx_Buffer_reset();
		goto FTPST;
	}

	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);


	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_CONFIG);
	gsm_user_write(FTP_CONFIG,strlen(FTP_CONFIG));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_PATH\r\n");
		user_debug("FTP_PATH NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_PATH NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();


	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTP_PATH);
	gsm_user_write(FTP_PATH,strlen(FTP_PATH));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTP_PATH\r\n");
		user_debug("FTP_PATH NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTP_PATH NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();



	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);

	gsm_timeout =1;
	configure_tcc(true,TC1,&gsm_inst,gsm_timeout_callback);
	user_debug(FTPSTATUS);
	gsm_user_write(FTPSTATUS,strlen(FTPSTATUS));
	while ((!GSM_check_acknowledge()) && (gsm_timeout>0));
	configure_tcc(false,TC1,&gsm_inst,gsm_timeout_callback);
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTPSTATUS\r\n");
		user_debug("FTPSTATUS NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTPSTATUS NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();

	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);

	gsm_timeout =1;
	GSM_rx_Buffer_reset();
	memset(ethernet_buffer,0,BUFFER_SIZE);
	copy_len = sprintf(ethernet_buffer,"%s",FTPGET);
	copy_len += sprintf(&ethernet_buffer[copy_len],"\"%s.bin\"\r",SERIAL_NO);
	user_debug("%s\n",ethernet_buffer);
	gsm_user_write(ethernet_buffer,strlen(ethernet_buffer));
	delay_s(3);
	if((gsm_timeout==0) || (rx_ack==2))
	{
		user_debug("Not FTPGET\r\n");
		user_debug("FTPGET NO: %s",rx_buffer);
		return 1;
	}
	user_debug("FTPGET NO: %s",rx_buffer);
	GSM_rx_Buffer_reset();
	delay_s(1);

	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);
	delay_s(3);

	user_debug("FTPRESP NO: %s\r\n",rx_buffer);
	if(strstr(rx_buffer,"-"))
	{
		user_debug("FTP Error: %s\r\n",rx_buffer);
		return 1;
	}


}

*/

	void gsm_timeout_callback(struct tc_module *const module_inst)
	{
		if(gsm_timeout>0)
		{
			gsm_timeout = gsm_timeout - 1;
		}
	}


