/*
 * gsm_api.c
 *
 * Created: 23/01/2018 12:17:39 AM
 *  Author: Arjun
 */ 

#include <main.h>
#include <gsm/GSM_M66.h>
#include "gsm/gsm_api.h"
#include <mqtt/publish_subscribe.h>


void gsm_init(void)
{
		#ifdef CONF_GSM
		configure_GSM_uart();
		#endif

	while(GSM_M66_init(mygsmdbg)>=1)
	{
		user_debug("%s\r\n",mygsmdbg);
		delay_s(2);
	}
	if(!get_network_time())
	{
		user_debug("Clock Initialization\r\n");
	}
}


void gsm_reinit(void)
{
	uint8_t ret_val;
	if(gsm_init_done != 0)
	{
		ret_val = GSM_M66_init(mygsmdbg);
		user_debug("%s\r\n",mygsmdbg);
		delay_s(2);
	}
	if(gsm_init_done == 0)
	{
		ret_val = get_network_time();
		if(ret_val == 0)
		{
			user_debug("Clock Initialization\r\n");
			gsm_reinit_status = 0;
			return 0;
		}
		return ret_val;							
	}
	return ret_val;
}



void gsm_send_data(uint8_t *data_payload, uint16_t data_payload_len)
{
	//uint8_t mqttip[4] = {52,65,126,193};
	//uint8_t mqttip[4] = {13,232,191,123}; //before 11jan
	uint8_t mqttip[4] = {13,126,235,58};	//11jan
	uint16_t plen= 0;
	int8_t send_retry;
	static uint8_t mqtt_type=0;
	static uint64_t data_rec_timeout = 0;
	switch (mqtt_type)
	{
		case 0:
			if(GSM_Socket_Open("TCP",mqttip,1883,0)==0)
			{
				user_debug("Server Initialized\r\n");
				memset(ethernet_buffer,0,BUFFER_SIZE);
				plen = custom_mqtt_connect(ethernet_buffer,0);
				user_debug("plen %d\r\n",plen);
				/************************************************************************/
				/*  MQTT  Connect                                                       */
				/************************************************************************/
				send_retry = 5;
				while(GSM_Send_data_to_Server(ethernet_buffer,plen) && send_retry>0)
				{
					user_debug("Data Send Issue\r\n");
					delay_s(2);
					send_retry = send_retry - 1;
				}
				if(send_retry>0)
				{
					mqtt_type =1;
					user_debug("send_retry %d\r\n",send_retry);
					user_debug("mqtt_type %d\r\n",mqtt_type);
				}
				else
				{
					mqtt_type = 0;
					user_debug("send_retry %d\r\n",send_retry);
					user_debug("mqtt_type %d\r\n",mqtt_type);
				}
				data_rec_timeout = ds1343_tick + 2;
			}
		break;		
		case 1:	
				user_debug("rx_wr_i %d\r\n",rx_wr_i);
				if(rx_wr_i > 2)
				{
					for(int i=0;i<rx_wr_i;i++)
					{
						user_debug(" %x ",rx_buffer[i]);
					}
					rx_mqtt_len = rx_wr_i;
					memset(ethernet_buffer,0,BUFFER_SIZE);
					plen = respond_for_request(rx_wr_i,ethernet_buffer,0,data_payload,data_payload_len);
					if(plen>0)
					{
						send_retry = 5;
						while(GSM_Send_data_to_Server(ethernet_buffer,plen) && send_retry>0)
						{
							user_debug("Data Send Issue\r\n");
							delay_s(2);
							send_retry = send_retry - 1;
						}
						if(send_retry>0)
						{
							mqtt_type =1;
							data_rec_timeout = ds1343_tick + 2;
						}
						else
						{
							mqtt_type = 0;
						}
					}
					else
					{
						mqtt_type = 0;
					}
					user_debug("plen %d\r\n",plen);					
				}
				else if(ds1343_tick > data_rec_timeout)
				{
					mqtt_type = 0;
					user_debug("GSM DATA REC Timeout\r\n");
				}
			/************************************************************************/
			/* MQTT Subscribe                                                       */
			/************************************************************************/

		break;

		default:
		break;
	}
}