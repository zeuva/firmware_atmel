/*
 * gsm_api.h
 *
 * Created: 23/01/2018 12:17:53 AM
 *  Author: Arjun
 */ 


#ifndef GSM_API_H_
#define GSM_API_H_
#include <asf.h>
#define SERVER_PORT_NtoHL	8080
uint8_t server_ip[4];

void gsm_init(void);
void gsm_reinit(void);
void gsm_send_data(uint8_t *data_payload, uint16_t data_payload_len);

#endif /* GSM_API_H_ */