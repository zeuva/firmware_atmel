/*
 * guage.c
 *
 * Created: 11/27/2017 9:07:13 PM
 *  Author: Owner
 */ 

#include "guage.h"
#include <main.h>
#include <flash/Flash_Api.h>

GAUGE_PKT fg_obj;

void guage_init(void)
{
	#ifdef CONF_I2C
		/* Initialize config structure and device instance. */
		struct i2c_master_config conf;
		i2c_master_get_config_defaults(&conf);		
		conf.baud_rate      = I2C_MASTER_BAUD_RATE_400KHZ;			
		conf.pinmux_pad0 = SDA;
		conf.pinmux_pad1 = SCL;

		/* Initialize and enable device with config. */
		i2c_master_init(&guage, I2C_MODULE, &conf);
		i2c_master_enable(&guage);
#endif
}

enum guage_i2c_status guage_write_command( uint8_t cmd)
{
	enum status_code i2c_status;
	uint8_t data[1];
	
	data[0] = cmd;
	
	struct i2c_master_packet transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 1,
		.data        = data,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	/* Do the transfer */
	i2c_status = i2c_master_write_packet_wait(&guage, &transfer);
	//user_debug("status %d\r\n",i2c_status);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;	
	return guage_i2c_status_ok;
}

void readfuelgauge()
{
	//uint8_t x = guage_read(GUAGE_ME);
	uint8_t soc = guage_read_soc();
	uint8_t MaxError = guage_read_ME();
	uint16_t RM = guage_read_RM();
	uint16_t volt = guage_read_volt();
	uint16_t fcc = guage_read_fcc();
	uint16_t ai = guage_read_ai();
	uint16_t ii = guage_read_ii();
	uint16_t ts = guage_read_ts();
	uint16_t cc = guage_read_cc();
	uint16_t soh = guage_read_soh();
	m_obj.gp.fg_temp = ts;
	m_obj.gp.fg_volt = volt;
	m_obj.gp.fg_soh = soh;
	//user_debug("SOC: %d\r\n",soc);
	//user_debug("Max Error: %d\r\n", MaxError);
	//user_debug("Remaining capacity: %d\r\n", RM);
	//user_debug("Module volt: %d mV\r\n", volt);
	//user_debug("Full charge capacity: %d\r\n", fcc);
	//user_debug("Average current: %d mA\r\n", ai);
	//user_debug("Temperature: %d /100 deg Celcius \r\n", ts);
	//user_debug("Cycle count: %d\r\n", cc);
	//user_debug("SOH: %d\r\n", soh);
}



uint8_t guage_read(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t *ptr_gp_1 = &m_obj.gp.fg_ctrl;
	uint16_t *ptr_gp_2 = &m_obj.gp.fg_atte;
	uint16_t *ptr_gp_3 = &m_obj.gp.fg_gn;
	uint8_t buffer[40] = {0};
	
	// Read data 
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 19,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_CNTL_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
		return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
		return guage_i2c_status_i2c_transfer_error;
	*ptr_gp_1++ = (buffer[1]<<8) | buffer[0];
	*ptr_gp_1++ = buffer[2];
	*ptr_gp_1++ = buffer[3];
	for(int i=2;i<10;i++)
	{
		*ptr_gp_1++ = (buffer[(i*2)+1]<<8) | buffer[i*2];	
	}	

	status = guage_write_command(AverageTimeToEmpty);
	if( status != guage_i2c_status_ok)
	return status;
	
	read_transfer.data_length=28;
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	for(int i=0;i<14;i++)
	{
		*ptr_gp_2++ = (buffer[(i*2)+1]<<8) | buffer[i*2];
	}

	status = guage_write_command(0x62);
	if( status != guage_i2c_status_ok)
	return status;
	
	read_transfer.data_length=14;
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	*ptr_gp_3++ = buffer[0];
	*ptr_gp_3++ = buffer[1];
	for(int i=1;i<6;i++)
	{
		*ptr_gp_3++ = (buffer[(i*2)+1]<<8) | buffer[i*2];
	}
	*ptr_gp_3 = (buffer[13]<<8) | buffer[12];

}

uint8_t guage_read_soc(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _soc;
	uint8_t buffer[1];	
	buffer[0] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 1,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_SOC);
	if( status != guage_i2c_status_ok)
		return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_soc = buffer[0];
	return _soc;
	
	}
	
uint8_t guage_read_ME(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _me;
	uint8_t buffer[1];
	
	buffer[0] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 1,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_ME);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_me = buffer[0];
	return _me;
	
}

uint16_t guage_read_RM(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _RM;
	uint8_t buffer[2];
	uint8_t _RM0, _RM1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_RM_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_RM0 = buffer[0];
	_RM1 = buffer[1];
	_RM = (_RM1<<8) | _RM0;
	return _RM;
	
}

uint16_t guage_read_fcc(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _fcc;
	uint8_t buffer[2];
	uint8_t _fcc0, _fcc1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_FCC_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_fcc0 = buffer[0];
	_fcc1 = buffer[1];
	_fcc = (_fcc1<<8) | _fcc0;
	return _fcc;
	
	}

uint16_t guage_read_volt(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _volt;
	uint8_t buffer[2];
	uint8_t _volt0, _volt1;
		
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_VOLT_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_volt0 = buffer[0];
	_volt1 = buffer[1];
	_volt = (_volt1<<8) | _volt0;
	return _volt;
	
}

uint16_t guage_read_ai(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _ai;
	uint8_t buffer[2];
	uint8_t _ai0, _ai1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_AI_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_ai0 = buffer[0];
	_ai1 = buffer[1];
	_ai = (_ai1<<8) | _ai0;
	return _ai;	
}

int16_t guage_read_ii(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	int16_t _ii;
	int8_t buffer[2];
	int8_t _ii0, _ii1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_II_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_ii0 = buffer[0];
	_ii1 = buffer[1];
	_ii = (_ii1<<8) | _ii0;
	
		return _ii;
	}
	
	


uint16_t guage_read_ts(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _ts;
	uint8_t buffer[2];
	uint8_t _ts0, _ts1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_TEMP_0);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	_ts0 = buffer[0];
	_ts1 = buffer[1];
	_ts = (_ts1<<8) | _ts0;
	return _ts;
	
}

uint16_t guage_read_cc(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _cc;
	uint8_t buffer[2];
	//uint8_t _ts0, _ts1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_CC);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	//_ts0 = buffer[0];
	//_ts1 = buffer[1];
	_cc = (buffer[1]<<8) | buffer[0];
	return _cc;
	
}

uint16_t guage_read_soh(void)
{
	enum guage_i2c_status status = guage_i2c_status_ok;
	enum status_code i2c_status;
	uint16_t _soh;
	uint8_t buffer[2];
	//uint8_t _ts0, _ts1;
	
	buffer[0] = 0;
	buffer[1] = 0;

	/* Read data */
	struct i2c_master_packet read_transfer = {
		.address     = GUAGE_ADDR,
		.data_length = 2,
		.data        = buffer,
		.ten_bit_address = false,
		.high_speed      = false,
		.hs_master_code  = 0x0,
	};
	
	status = guage_write_command(GUAGE_SOH);
	if( status != guage_i2c_status_ok)
	return status;
	
	i2c_status = i2c_master_read_packet_wait(&guage, &read_transfer);
	if( i2c_status == STATUS_ERR_OVERFLOW )
	return guage_i2c_status_no_i2c_acknowledge;
	if( i2c_status != STATUS_OK)
	return guage_i2c_status_i2c_transfer_error;
	//_ts0 = buffer[0];
	//_ts1 = buffer[1];
	_soh = (buffer[1]<<8) | buffer[0];
	return _soh;
	
}

void pulse_overcurrent_callback(struct tc_module *const module_inst)
{
	delay_us(1);
	/*if(toggle)
	{
		Discharge_ON();
		toggle = 0;	
		
			}
	else
	{
		Discharge_OFF();
		toggle = 1;
	}*/
}
