/*
 * guage.h
 *
 * Created: 11/27/2017 9:06:16 PM
 *  Author: Owner
 */ 


#ifndef GUAGE_H_
#define GUAGE_H_

#include <asf.h>

typedef struct {
	uint16_t fg_ctrl;
	uint16_t fg_soc;
	uint16_t fg_me;	
	uint16_t fg_rm;
	uint16_t fg_fcc;
	uint16_t fg_volt;
	uint16_t fg_ai;
	uint16_t fg_temp;
	uint16_t fg_flags;
	uint16_t fg_i;
	uint16_t fg_flagsb;
	
	uint16_t fg_atte;
	uint16_t fg_attf;
	uint16_t fg_pchg;
	uint16_t fg_dodot;
	uint16_t fg_ae;
	uint16_t fg_ap;
	uint16_t fg_sernum;
	uint16_t fg_inttemp;
	uint16_t fg_cc;
	uint16_t fg_soh;
	uint16_t fg_chgv;
	uint16_t fg_chgi;
	uint16_t fg_pkcfg;
	uint16_t fg_dcap;

	uint16_t fg_gn;
	uint16_t fg_ls;
	uint16_t fg_deoc;
	uint16_t fg_qs;
	uint16_t fg_trc;
	uint16_t fg_tfcc;
	uint16_t fg_st;
	uint16_t fg_qpc;
}GAUGE_PKT;

struct i2c_master_module guage;


enum guage_i2c_status {
	guage_i2c_status_ok,
	guage_i2c_status_no_i2c_acknowledge,
	guage_i2c_status_i2c_transfer_error,
	guage_i2c_status_crc_error
};

#define GUAGE_ADDR 0x55

// Commands
#define GUAGE_CNTL_0 0x00
#define GUAGE_CNTL_1 0x01
#define GUAGE_SOC    0x02   // State of charge
#define GUAGE_ME     0x03   // MaxError
#define GUAGE_RM_0   0x04   // Remaining capacity
#define GUAGE_RM_1   0x05
#define GUAGE_FCC_0  0x06   // Full Charge Capacity
#define GUAGE_FCC_1  0x07  
#define GUAGE_VOLT_0 0x08	// Voltage
#define GUAGE_VOLT_1 0x09
#define GUAGE_AI_0   0x0A	// Average voltage
#define GUAGE_AI_1   0x0B	
#define GUAGE_TEMP_0 0x0C	// Temperature 0.1/K
#define GUAGE_TEMP_1 0x0D
#define GUAGE_II_0	 0x10	// Instantaneous current
#define GUAGE_II_1	 0x11
#define GUAGE_ENERGY 0x24	// Available energy in the pack
#define GUAGE_POWER  0x26	// Average power 
#define GUAGE_INTTEMP 0x2A	// Internal temperature of fuel gauge
#define GUAGE_CC	 0x2C	// Cycle count
#define GUAGE_SOH	 0x2E	// State of health of the battery
#define DESIGN_CAP	 0x3C	// Design capacity
#define AverageTimeToEmpty	0x18	// Average time to empty
#define AverageTimeToFull	0x1A	// Average time to full the battery




void guage_init(void);
uint8_t guage_read(void);
void readfuelgauge();
enum guage_i2c_status guage_write_command( uint8_t cmd);
uint8_t guage_read_soc(void);
uint8_t guage_read_ME(void);
uint16_t guage_read_RM(void);
uint16_t guage_read_volt(void);
uint16_t guage_read_fcc(void);
uint16_t guage_read_ai(void);
uint16_t guage_read_ts(void);
uint16_t guage_read_cc(void);
uint16_t guage_read_soh(void);
int16_t guage_read_ii(void);
//uint8_t guage_read(uint8_t cmd);
//guage_read(uint16_t parameter, uint8_t cmd);
void pulse_overcurrent_callback(struct tc_module *const module_inst);
#endif /* GUAGE_H_ */