/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to system_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <main.h>
#include <stdio.h>
#include <timer/timeout.h>
#include "Internal_eeprom/emulated_eeprom.h"
#include <usercalendar/user_calendar.h>
struct calendar_date date_usr;

uint64_t ds1343_tick;
uint64_t last_ds1343_tick;
void initialization (void)
{
	irq_initialize_vectors();
	cpu_irq_enable();
	system_init();
	delay_init();

	#ifdef CONF_DEBUG
		debug_uart_init();
	#endif

	configure_eeprom();
  
	#ifdef CONF_FLASH
		#ifdef CONF_SPI
			flash_init();
			flash_address_init();
		#endif
	#endif
	
	#ifdef CONF_I2C
		guage_init();
	#endif


	#ifdef CONF_SCI
		configure_SCI_uart();
		bq76pl455_init();
	#endif


}

uint32_t volatile usertimestamp = 1519990200;
uint32_t volatile readtimestamp = 1519995600;
void timestamp_callback(struct tc_module *const module_inst);
void calendar_to_device_calendar(uint16_t *dp_ptr,struct calendar_date *date_ptr, bool copy);
uint16_t raw_data_to_Json(void);
struct tc_module usr_inst;

int main (void)
{
	int cycle=1;
	uint16_t data_len=0;
	uint8_t record_interval = 30;
	initialization();
	user_debug("Hello");
	//gsm_init();	
	configure_extint_channel();		
	configure_extint_callbacks();											//"commented on 20/12/2018"
	configure_tcc(true,TC0,&usr_inst,timestamp_callback);
	while(1) 
	{
		
		user_debug("\n\ncycles %d\n",cycle++);
		
		read_all_faults();
		user_debug("\n");
		delay_ms(100);

		readvoltage();
		user_debug("\n");
		delay_ms(100);

		guage_read();
		delay_ms(100);

		if(ds1343_tick > last_ds1343_tick)
		{
			flash_buffer_write(&m_obj,sizeof(GLOBAL_PKT));
			last_ds1343_tick = ds1343_tick + record_interval; 
			delay_ms(100);			
		}
		//data_len = raw_data_to_Json();
		//user_debug(" Data: %s\r\n", data_buffer);

		/*if(gsm_reinit_status || gsm_init_done)								//"commented on 20/12/2018"
		{
			gsm_reinit();
		}
		else
		{
			data_len = raw_data_to_Json();
			user_debug("GSM Send Data: %s\r\n", data_buffer);
			gsm_send_data(data_buffer,data_len);
		}*/
		
/*		user_debug("Write TEMP %d\r\n",m_obj.gp.fg_temp);
		user_debug("Write Volt %d\r\n",m_obj.gp.fg_volt);
		user_debug("Write SOH %d\r\n",m_obj.gp.fg_soh);
		user_debug("Write CELL1  %d V\r\n",m_obj.cv.cell_volt1);
		user_debug("Write CELL2  %d V\r\n",m_obj.cv.cell_volt2);
		user_debug("Write CELL3  %d V\r\n",m_obj.cv.cell_volt3);
		user_debug("Write CELL4  %d V\r\n",m_obj.cv.cell_volt4);
		user_debug("Write CELL5  %d V\r\n",m_obj.cv.cell_volt5);
		user_debug("Write CELL6  %d V\r\n",m_obj.cv.cell_volt6);
		user_debug("Write CELL7  %d V\r\n",m_obj.cv.cell_volt7);
		user_debug("Write CELL8  %d V\r\n",m_obj.cv.cell_volt8);
		user_debug("Write CELL9  %d V\r\n",m_obj.cv.cell_volt9);
		user_debug("Write CELL10  %d V\r\n",m_obj.cv.cell_volt10);
		user_debug("Write CELL11  %d V\r\n",m_obj.cv.cell_volt11);
		user_debug("Write CELL12  %d V\r\n",m_obj.cv.cell_volt12);
		user_debug("Write CELL13  %d V\r\n",m_obj.cv.cell_volt13);
		user_debug("Write CELL14  %d V\r\n",m_obj.cv.cell_volt14);
		user_debug("Write CELL15  %d V\r\n",m_obj.cv.cell_volt15);
		user_debug("Write CELL16  %d V\r\n",m_obj.cv.cell_volt16);
*/
	}
}

void timestamp_callback(struct tc_module *const module_inst)
{
	ds1343_tick = ds1343_tick + 2;
	user_debug("timestamp_callback %ld\r\n",ds1343_tick);
	calendar_timestamp_to_date(ds1343_tick,&date_usr);
	calendar_to_device_calendar(&m_obj.ud,&date_usr,true);	
}


/*********************************************************************************************************
*	Updating the Data packet time & date from the calendar structure module.
*	Parameter_1: structure data_packet object pointer for updating time & date.
*	Parameter_2: structure calendar_date from calendar module which provide date & time.
*	Parameter_3: True => Copy from Calendar_date to data_packet
*			   : False => Copy From Device Configuration to Calendar_date.
*********************************************************************************************************/
void calendar_to_device_calendar(uint16_t *dp_ptr,struct calendar_date *date_ptr, bool copy)
{
	if(copy)
	{
		dp_ptr[0]=(date_ptr->hour);
		dp_ptr[1]=(date_ptr->minute);
		dp_ptr[2]=(date_ptr->second);
		dp_ptr[3]=(date_ptr->date)+1;
		dp_ptr[4]=(date_ptr->month)+1;
		dp_ptr[5]=date_ptr->year;		// only one byte is allocated for year. so subtracted by current century.
	}
	else
	{
		date_ptr->hour=dp_ptr[0];
		date_ptr->minute=dp_ptr[1];
		date_ptr->second=dp_ptr[2];
		date_ptr->date=dp_ptr[3]-1;
		date_ptr->month=dp_ptr[4]-1;
		date_ptr->year=dp_ptr[5];
	}
}


uint16_t raw_data_to_Json(void)
{	
	uint16_t json_data_len = 0; 
	uint16_t *ptr_rd_bms_volt = &(m_obj.bd.bms_cv.cell_volt1);					//changed from rd_obj to m_obj 12/1/2019
	uint16_t *ptr_rd_bms_temp = &(m_obj.bd.bms_ct.cell_temp1);
	uint16_t *ptr_rd_bms_fault = &(m_obj.bd.bms_bf.fault_sum);
	uint16_t *ptr_rd_gauge = &(m_obj.gp.fg_ctrl);
	flash_buffer_read(&m_obj,sizeof(GLOBAL_PKT));

	json_data_len = sprintf(&data_buffer[json_data_len],"{Device_id : 1122334455, ");
	json_data_len = json_data_len + sprintf(&data_buffer[json_data_len],"{Time: %d:%d:%d},",m_obj.ud.hour,m_obj.ud.minute,m_obj.ud.second);
	json_data_len = json_data_len + sprintf(&data_buffer[json_data_len],"{Date: %d-%d-%d},",m_obj.ud.date,m_obj.ud.month,m_obj.ud.year);
	json_data_len += sprintf(&data_buffer[json_data_len],"{BMS DATA: {BMS Battery Volt: ");
	for(int i = 1;i<=15;i++)
	{
		json_data_len += sprintf(&data_buffer[json_data_len],"%d V%d, ",*ptr_rd_bms_volt++,i);
		user_debug("%d V%d, ",*ptr_rd_bms_volt,i);	
	}
	json_data_len += sprintf(&data_buffer[json_data_len],"%d V16}, {BMS Battery Temp: ",m_obj.bd.bms_cv.cell_volt16);
	for(int i = 1;i<=5;i++)
	{
		json_data_len += sprintf(&data_buffer[json_data_len],"%d T%d, ",*ptr_rd_bms_temp++,i);
	}
	json_data_len += sprintf(&data_buffer[json_data_len],"%d T6 }, BMS Fault: ",m_obj.bd.bms_ct.cell_temp6);
	for(int i = 1;i<10;i++)
	{
		json_data_len += sprintf(&data_buffer[json_data_len],"Fault%d = %x, ",i,*ptr_rd_bms_fault++);
	}
	json_data_len += sprintf(&data_buffer[json_data_len],"Fault10 = %x} }, {Guage Data: ",*ptr_rd_bms_fault);
	for(int i = 1;i<33;i++)
	{
		json_data_len += sprintf(&data_buffer[json_data_len],"%x, ",*ptr_rd_bms_fault++);
	}
	json_data_len += sprintf(&data_buffer[json_data_len],"%x } }",*ptr_rd_bms_fault);
	return json_data_len;
}