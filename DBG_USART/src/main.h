/*
 * main.h
 *
 * Created: 11/25/2017 10:43:25 AM
 *  Author: Owner
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#include <debug/debug.h>
#include <flash/Flash_Api.h>
#include <guage/guage.h>
#include <gsm/gsm_api.h>
#include <bms/pl455.h>
#include <fault_interrupt/fault_interrupt.h>
extern uint64_t ds1343_tick;

typedef struct {
	uint16_t hour;          //!< 0-23
	uint16_t minute;        //!< 0-59
	uint16_t second;        //!< 0-59
	uint16_t date;          //!< 0-30 \note First day of month is 0, not 1.
	uint16_t month;         //!< 0 January - 11 December
	uint16_t year;         //!< 1970-2105
}USER_DATE;




typedef struct {
	USER_DATE ud;
	BMS_DATA bd;
	GAUGE_PKT gp;
}GLOBAL_PKT;

GLOBAL_PKT m_obj;
GLOBAL_PKT rd_obj;
#ifdef CONF_DEBUG
	#define user_debug(...)     printf(__VA_ARGS__)
	#else
	#define user_debug(...)
	#endif

void initialization (void);
#define BUFFER_SIZE 1100
uint8_t rx_buffer[300];
uint16_t rx_mqtt_len;
uint8_t ethernet_buffer[BUFFER_SIZE+100];
uint8_t data_buffer[BUFFER_SIZE+10];
struct __attribute__((__packed__)) gsm_config
{
	uint8_t volatile rtclock_time[6];
	uint8_t volatile gsm_imei[16];
	uint8_t sim_ccid[20];
	uint32_t gms_baud;
	uint8_t gsm_parity;
	uint8_t gsm_stop;
	uint8_t gsm_char_len;
	uint8_t gsm_apn[32];
	uint8_t gsm_username[32];
	uint8_t gsm_pass[32];
};

struct __attribute__((__packed__)) gsm_tmp_conf
{
	char volatile gsm_imei[16];
	char sim_ccid[20];
	char gsm_apn[32];
};

uint8_t gsm_init_done;
uint8_t gsm_reinit_status;
uint8_t gsminit_retry;
uint8_t gsmconn_retry;
uint8_t mygsmdbg[50];

struct gsm_config gsm_obj;
struct gsm_tmp_conf gsm_tmp;


#endif /* MAIN_H_ */