#include <asf.h>
#include "mqtt/MQTTPacket.h"
#include <flash/Flash_Api.h>
uint16_t rx_buflen =0;
uint32_t app_counter;

int getdata(char* buf, int count)
{
//#ifdef MQTT
	user_debug("getdata\r\n");
	if(rx_mqtt_len>=(rx_buflen+count))
	{
		for (int qq=0;qq<count;qq++)
		{
			buf[qq]=rx_buffer[rx_buflen];
			rx_buflen++;
		}		
	}
	else
	{
		count = 0;
	}
//#endif
    return count;
}
 
int toStop = 0;
 
 
uint16_t custom_mqtt_connect(uint8_t *buf,uint8_t buf_offset)
{
	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
	int rc = 0;
	int msgid = 1;
	MQTTString topicString = MQTTString_initializer;
	int req_qos = 1;
	char* payload = "mypayload";
	int payloadlen = strlen(payload);
	int len = 0;

	data.clientID.cstring = "Zeuva_Test";
	data.keepAliveInterval = 60;
	data.cleansession = 1;
	data.will.qos = 1;
	data.username.cstring = "";
	data.password.cstring = "";
	return MQTTSerialize_connect(&buf[buf_offset], 650, &data);
	 
}
 
uint16_t respond_for_request(uint16_t rx_len,uint8_t *buf,uint8_t offset,uint8_t *data_payload, uint16_t data_payload_len)
{
//#ifdef MQTT
	uint8_t mqtt_msg_type;
	rx_buflen=0;
	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
	int rc = 0;
	int msgid = 1;
	unsigned char sessionPresent;
	MQTTString topicString = MQTTString_initializer;
	int req_qos = 0;
	int payloadlen;
	int len = 0;
	app_counter = app_counter + 1;
	data.clientID.cstring = "Zeuva_Test";
	data.keepAliveInterval = 20;
	data.cleansession = 1;
	/* wait for connack */
	mqtt_msg_type= MQTTPacket_read(rx_buffer, rx_len, getdata);
	if (mqtt_msg_type == CONNACK)
	{
		char connack_rc;
		user_debug("MQTT CONNECT ACK\r\n");
		if (MQTTDeserialize_connack(&sessionPresent,&connack_rc, rx_buffer, rx_len) != 1 || connack_rc != 0)
		{
			printf("Unable to connect, return code %d\n", connack_rc);
			goto exit;
		}
		/* subscribe */
		topicString.cstring = "substopic";
		return MQTTSerialize_subscribe(&buf[offset],BUFFER_SIZE-offset, 0, msgid, 1, &topicString, &req_qos);
	}
	else if (mqtt_msg_type == SUBACK)    /* wait for suback */
	{
		int submsgid;
		int subcount;
		int granted_qos;
		
		user_debug("MQTT SUBSCRIPTION ACK\r\n");
		rc = MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos,rx_buffer, rx_len);
		if (granted_qos != 0)
		{
			printf("granted qos != 0, %d\n", granted_qos);
			goto exit;
		}
		topicString.cstring = "counter";
		printf("publishing reading %d\r\n",data_payload_len);
		return MQTTSerialize_publish(&buf[offset],BUFFER_SIZE-offset, 0, 1, 0, 34, topicString, data_payload, data_payload_len);
	}
	else if (mqtt_msg_type == PUBACK)
	{
		int dup;
		int qos;
		int retained;
		int msgid;
		int payloadlen_in;
		char* payload_in;
		int rc;
		MQTTString receivedTopic;
		user_debug("MQTT PUBLISH\r\n");

		rc = MQTTDeserialize_publish(&dup, &qos, &retained, &msgid, &receivedTopic,&payload_in, &payloadlen_in,rx_buffer, rx_len);

		if(rc>0)
		{
			printf("message arrived %d => %s\n", payloadlen_in, payload_in);		
		}
		return MQTTSerialize_pingreq(&buf[offset],BUFFER_SIZE-offset);
	}
	else if (mqtt_msg_type == PINGRESP)
	{
		user_debug("MQTT PING RESPONSE\r\n");
		topicString.cstring = "counter";
		printf("publishing reading PINGRESP %d\r\n",data_payload_len);
		return MQTTSerialize_publish(&buf[offset],BUFFER_SIZE-offset, 0, 1, 0, 34, topicString, data_payload, data_payload_len);
	}
	else if(mqtt_msg_type == DISCONNECT)
	{
		return custom_mqtt_connect(&buf[offset],BUFFER_SIZE-offset);
	}
	exit:
//#endif
	return 0;
 
}
 