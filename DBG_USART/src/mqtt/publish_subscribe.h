/*
 * publish_subscribe.h
 *
 * Created: 03/03/2018 5:03:42 PM
 *  Author: Arjun
 */ 

#include <asf.h>
#ifndef PUBLISH_SUBSCRIBE_H_
#define PUBLISH_SUBSCRIBE_H_

int getdata(char* buf, int count);

uint16_t custom_mqtt_connect(uint8_t *buf,uint8_t buf_offset);

uint16_t respond_for_request(uint16_t rx_len,uint8_t *buf,uint8_t offset,uint8_t *data_payload, uint16_t data_payload_len);



#endif /* PUBLISH_SUBSCRIBE_H_ */