#include <asf.h>
#include "MQTTPacket.h"
#include <main.h>
int getdata(char* buf, int count)
{
	for (int qq=0;qq<count;qq++)
	{
		buf[qq]=rx_buffer[rx_buflen];
		rx_buflen++;
	}
    return count;
}
 
int toStop = 0;
 
 
 uint16_t custom_mqtt_connect(uint8_t *buf,uint8_t buf_offset)
 {
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
    int rc = 0;
    int msgid = 1;
    MQTTString topicString = MQTTString_initializer;
    int req_qos = 0;
    char* payload = "mypayload";
    int payloadlen = strlen(payload);
    int len = 0;

    data.clientID.cstring = "SendReceive mbed MQTT ";
    data.keepAliveInterval = 10;
    data.cleansession = 1;
    return MQTTSerialize_connect(&ethernet_buffer[buf_offset], BUFFER_SIZE-buf_offset, &data);
	 
 }
 
uint16_t respond_for_request(uint16_t rx_len,uint8_t *buf,uint8_t offset)
{
	uint8_t mqtt_msg_type;
	rx_buflen=0;
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
    int rc = 0;
    int msgid = 1;
	unsigned char sessionPresent;
    MQTTString topicString = MQTTString_initializer;
    int req_qos = 0;
    char* payload = "mypayload";
    int payloadlen = strlen(payload);
    int len = 0;

    data.clientID.cstring = "SendReceive mbed MQTT ";
    data.keepAliveInterval = 20;
    data.cleansession = 1;
    /* wait for connack */
	mqtt_msg_type= MQTTPacket_read(rx_buffer, rx_len, getdata);
    if (mqtt_msg_type == CONNACK)
    {
        char connack_rc;
		user_debug_1("MQTT CONNECT ACK\r\n");
        if (MQTTDeserialize_connack(&sessionPresent,&connack_rc, rx_buffer, rx_len) != 1 || connack_rc != 0)
        {
            printf("Unable to connect, return code %d\n", connack_rc);
            goto exit;
        }
		/* subscribe */
		topicString.cstring = "substopic";
		return MQTTSerialize_subscribe(&buf[offset],BUFFER_SIZE-offset, 0, msgid, 1, &topicString, &req_qos);
    }
    else if (mqtt_msg_type == SUBACK)    /* wait for suback */
    {
        int submsgid;
        int subcount;
        int granted_qos;
 
		user_debug_1("MQTT SUBSCRIPTION ACK\r\n");
        rc = MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos,rx_buffer, rx_len);
        if (granted_qos != 0)
        {
            printf("granted qos != 0, %d\n", granted_qos);
            goto exit;
        }
		topicString.cstring = "pubtopic";
		printf("publishing reading\n");
		return MQTTSerialize_publish(&buf[offset],BUFFER_SIZE-offset, 0, 1, 0, 34, topicString, payload, payloadlen);	
    }
    else if (mqtt_msg_type == PUBLISH)
    {
        int dup;
        int qos;
        int retained;
        int msgid;
        int payloadlen_in;
        char* payload_in;
        int rc;
        MQTTString receivedTopic;
 		user_debug_1("MQTT PUBLISH\r\n");

        rc = MQTTDeserialize_publish(&dup, &qos, &retained, &msgid, &receivedTopic,&payload_in, &payloadlen_in,rx_buffer, rx_len);
        printf("message arrived %d => %s\n", payloadlen_in, payload_in);
    }
    else if (mqtt_msg_type == PINGRESP)
    {
	    user_debug_1("MQTT PING RESPONSE\r\n");
    }
	
	exit:
	return 0;
 
}
 