/*
 * timeout.c
 *
 * Created: 21-05-2015 10:51:07
 *  Author: User
 */ 

#include <timer/timeout.h>
#include <guage/guage.h>
#include <bms/pl455.h>

#define CONF_TC_MODULE3 TC3
#define CONF_TC_MODULE4 TC4
#define CONF_TC_MODULE4 TC5

struct tc_module dchgOT_instance;
struct tc_module chgOT_instance;
struct tc_module OC_instance;

int16_t ii;


//! [module_inst]
//! [callback_funcs]
//! [callback_funcs]
//! [setup]

void configure_tc(void)
{
	struct tc_config config_tc;
	tc_get_config_defaults(&config_tc);
	config_tc.counter_size = TC_COUNTER_SIZE_8BIT;
	config_tc.clock_source = GCLK_GENERATOR_1;
	config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV1024;
	config_tc.counter_8_bit.period = 100;
	tc_init(&dchgOT_instance, CONF_TC_MODULE3, &config_tc);
	tc_init(&chgOT_instance, CONF_TC_MODULE4, &config_tc);
	tc_init(&OC_instance, CONF_TC_MODULE4, &config_tc);
	//tc_enable(&tc_instance);
}

//! [setup]
void configure_tcc(bool timer_state,Tc *const hw,struct tc_module *tc_inst,tc_callback_t callback_func)
{
	//! [setup_config]
	if(timer_state)
	{
		struct tc_config config_tc;
		//! [setup_config]

		//! [setup_config_defaults]
		tc_get_config_defaults(&config_tc);
		//! [setup_config_defaults]

		//! [setup_change_config]
		config_tc.clock_source = GCLK_GENERATOR_2;
		config_tc.counter_size    = TC_COUNTER_SIZE_8BIT;
		config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV256;
		config_tc.counter_8_bit.period = 0xFF;
		//! [setup_change_config]

		//! [setup_set_config]
		tc_init(tc_inst, hw, &config_tc);
		//! [setup_set_config]

		//! [setup_enable]
		tc_enable(tc_inst);
		configure_tcc_callbacks(tc_inst,callback_func);
		//! [setup_enable]		
	}
	else
	{
		tc_disable(tc_inst);	
		tc_disable_callback(tc_inst, TC_CALLBACK_OVERFLOW);	
	}
}

void configure_tcc_callbacks(struct tc_module *tc_inst,tc_callback_t callback_func)
{
	//! [setup_register_callback]
	tc_register_callback(tc_inst, callback_func,TC_CALLBACK_OVERFLOW);
//	user_debug_1("CALLBACK No: %d\r\n",TCC_CALLBACK_N);
	//! [setup_enable_callback]
	tc_enable_callback(tc_inst, TC_CALLBACK_OVERFLOW);
	//! [setup_enable_callback]
}

//
//void tc_callback_dchgOT_instance(struct tc_module *const module_inst)
//{
	////port_pin_toggle_output_level(DISCHARGING);
	//readvoltage();
	//tc_disable(&dchgOT_instance);
	//if (FLT_AUX > 0)
	//{
		//Charging_OFF();
	//}
	//else
	//{
		//Charging_ON();
	//} 
//}
//
//
//void tc_callback_chgOT_instance(struct tc_module *const module_inst)
//{
	////port_pin_toggle_output_level(DISCHARGING);
	//read_faults();
	//tc_disable(&chgOT_instance);
	//if (FLT_AUX > 0)
	//{
		//Discharge_OFF();
	//}
	//else
	//{
		//Discharge_ON();
	//}	
//}
//
//
//void tc_callback_OC_instance(struct tc_module *const module_inst)
//{
//
	////port_pin_toggle_output_level(DISCHARGING);
	//tc_disable(&OC_instance);
	//if (ii > 0)
	//{
		//Discharge_OFF();
	//}
	//else
	//{
		//Discharge_ON();
	//}
//}
//
//
//void configure_tc_callbacks(void)
//{
	//tc_register_callback(&dchgOT_instance, tc_callback_dchgOT_instance,	TC_CALLBACK_OVERFLOW);
	//tc_register_callback(&chgOT_instance, tc_callback_chgOT_instance,	TC_CALLBACK_OVERFLOW);
	//tc_enable_callback(&dchgOT_instance, TC_CALLBACK_OVERFLOW);
	//tc_enable_callback(&chgOT_instance, TC_CALLBACK_OVERFLOW);
//}