/*
 * timeout.h
 *
 * Created: 21-05-2015 10:50:22
 *  Author: User
 */ 

#include <asf.h>

#ifndef TIMEOUT_H_
#define TIMEOUT_H_


void configure_tcc(bool timer_state,Tc *const hw,struct tc_module *tc_inst,tc_callback_t callback_func);
void configure_tcc_callbacks(struct tc_module *tc_inst,tc_callback_t callback_func);
void configure_tc(void);
void configure_tc_callbacks(void);
void tc_callback_dchgOT_instance(struct tc_module *const module_inst);
void tc_callback_chgOT_instance(struct tc_module *const module_inst);

uint8_t timeout_period;
//! [module_inst]



#endif /* TIMEOUT_H_ */